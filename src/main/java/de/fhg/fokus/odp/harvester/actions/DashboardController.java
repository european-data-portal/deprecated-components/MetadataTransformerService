package de.fhg.fokus.odp.harvester.actions;

import de.fhg.fokus.odp.harvester.persistence.HarvestersManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import org.picketlink.Identity;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * Created by sim on 26.06.2017.
 */
@Named
@ViewScoped
public class DashboardController implements Serializable {

    @Inject
    private Identity identity;

    @Inject
    private HarvestersManager harvestersManager;

    private List<Harvester> recentlyFinished;
    private List<Harvester> running;
    private List<Harvester> scheduled;

    @Transactional
    public void load() {
        recentlyFinished = harvestersManager.listRecentlyFinishedHarvesters(3);
        running = harvestersManager.listRunningHarvesters();
        scheduled = harvestersManager.listScheduledHarvesters(5);
    }

    public List<Harvester> listRunningHarvesters() {
        return running;
    }

    public List<Harvester> listScheduledHarvesters() {
        return scheduled;
    }

    public List<Harvester> listRecentlyFinishedHarvesters() {
        return recentlyFinished;
    }

    public int countUserHarvesters() {
        return harvestersManager.countUserHarvesters().intValue();
    }

    @Transactional
    public void refresh() {
        recentlyFinished = harvestersManager.listRecentlyFinishedHarvesters(3);
        running = harvestersManager.listRunningHarvesters();
        scheduled = harvestersManager.listScheduledHarvesters(5);
    }
}
