package de.fhg.fokus.odp.harvester.actions;

import de.fhg.fokus.odp.harvester.application.AppLocalization;
import de.fhg.fokus.odp.harvester.exceptions.ValidationException;
import de.fhg.fokus.odp.harvester.persistence.HarvestersManager;
import de.fhg.fokus.odp.harvester.persistence.LogsManager;
import de.fhg.fokus.odp.harvester.persistence.RepositoriesManager;
import de.fhg.fokus.odp.harvester.persistence.RunsManager;
import de.fhg.fokus.odp.harvester.persistence.entities.*;
import de.fhg.fokus.odp.harvester.persistence.enums.Frequency;
import de.fhg.fokus.odp.harvester.persistence.enums.HarvesterEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.RunState;
import de.fhg.fokus.odp.harvester.persistence.enums.Visibility;
import de.fhg.fokus.odp.harvester.service.scheduler.QuartzSchedulerService;
import de.fhg.fokus.odp.harvester.util.TransformationScriptDownloader;
import de.fhg.fokus.odp.harvester.util.xml.HarvesterXmlHandler;
import org.apache.commons.io.IOUtils;
import org.picketlink.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.transaction.Transactional;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

@Named
@ViewScoped
public class HarvesterAction implements Serializable {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private static final long serialVersionUID = 7065759473371058551L;

	@Inject
	private Identity identity;

	@Inject
	private HarvestersManager harvestersManager;

	@Inject
	private RunsManager runsManager;

	@Inject
	private LogsManager logsManager;

	@Inject
	private RepositoriesManager repositoriesManager;

	@Inject
	private QuartzSchedulerService quartzSchedulerService;

	private Harvester harvester;

	private TransformationScriptDownloader.GitRepository gitRepo;

	private String script;

	private Part importedHarvesterFile;

	private Integer id;

	@Transactional
	public void load() {
		if (id != null) {
			harvester = harvestersManager.find(id);
			if (harvester.getTransformationScriptSource() == HarvesterEnums.TransformationScriptSource.git) {
				gitRepo = new TransformationScriptDownloader.GitRepository(
						harvester.getGitRepoUrl(),
						harvester.getGitBranch(),
						harvester.getGitFileName(),
						harvester.isGitRepoRequiresCredentials(),
						harvester.isGitRepoSshAuthentication(),
						harvester.getGitUserName(),
						harvester.getGitPassWord());
				script = new TransformationScriptDownloader().initScriptDownloader(gitRepo).getCurrentScript(true);
			} else {
			    script = harvester.getMappingScript();
            }
		} else {
			harvester = new Harvester();
			harvester.setOwner(identity.getAccount().getId());
			harvester.setVisibility(Visibility.private_visibility);
			harvester.setFrequency(Frequency.manually);
			harvester.setTransformationScriptSource(HarvesterEnums.TransformationScriptSource.custom);
			script = harvester.getMappingScript();
		}
	}

	@Transactional
	public void save() {
		if (id != null) {
			harvester = harvestersManager.update(harvester);
		}

		// check filters for existing values
		boolean filtersValid = true;
		for (Filter f : harvester.getFilters()) {
			if (f.getAttribute().isEmpty() || f.getValue().isEmpty()) {
				filtersValid = false;
				break;
			}
		}

		if (id == null && harvestersManager.nameExists(harvester.getName())) {
			AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrNameExists", null);
		} else if(!filtersValid) {
			AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrFilterInvalid", null);
		} else {
			if (id == null) {
				if (harvester.getSampleTransformationFileName() != null) {
					try {
						InputStream scriptInput = Thread.currentThread().getContextClassLoader().getResourceAsStream(harvester.getSampleTransformationFileName());
						if (scriptInput != null) {
							String script = IOUtils.toString(scriptInput, "UTF-8");
							harvester.setMappingScript(script);
						}
					} catch (IOException e) {
						log.error("sample script", e);
					}
				}

				harvestersManager.persist(harvester);
				id = harvester.getId();
				AppLocalization.showMessage(FacesMessage.SEVERITY_INFO, "msgInfoHarvesterCreated", null);
			} else {
				AppLocalization.showMessage(FacesMessage.SEVERITY_INFO, "msgInfoHarvesterUpdated", null);
			}

			if (harvester.getRuns().stream().noneMatch(r -> r.getRunState() == RunState.running)) {
				if (harvester.getFrequency() != Frequency.manually) {
					Date date = quartzSchedulerService.scheduleJob(harvester.getId().toString(), harvester.getFrequency(), harvester.getNextHarvestDate());
					if (date != null) {
						harvester.setNextHarvestDate(date);
					}
				} else {
					quartzSchedulerService.deleteJob(harvester.getId().toString());
				}
			}

			//Remove git data if not needed anymore
			if (harvester.getTransformationScriptSource().equals(HarvesterEnums.TransformationScriptSource.custom)) {

				//Delete files only if not used anymore
				if (gitRepo != null && !harvestersManager.isGitRepoInUse(harvester.getGitRepoUrl())) {
					new TransformationScriptDownloader().initScriptDownloader(gitRepo).removeGitFiles();
				}

				harvester.setGitRepoUrl(null);
				harvester.setGitBranch(null);
				harvester.setGitFileName(null);
				harvester.setGitRepoRequiresCredentials(false);
			}

			//Remove git credentials if not needed anymore
			if (!harvester.isGitRepoRequiresCredentials()) {
				harvester.setGitUserName(null);
				harvester.setGitPassWord(null);
			}

//			harvester = harvestersManager.update(harvester);
		}
	}

	//Delete Harvester from within Harvester itself. Redirect to Dashboard
	@Transactional
	public String delete() {
		quartzSchedulerService.deleteJob(harvester.getId().toString() + (harvester.getFrequency() == Frequency.manually ? ".manually" : ""));

		if (harvester.getSource() != null) {
			harvester.getSource().getHarvesterSource().remove(harvester);
//			harvester.setSource(repositoriesManager.update(harvester.getSource()));
		}

		if (harvester.getTarget() != null) {
			harvester.getTarget().getHarvesterTarget().remove(harvester);
//			harvester.setTarget(repositoriesManager.update(harvester.getTarget()));
		}

		if (gitRepo != null && !harvestersManager.isGitRepoInUse(harvester.getGitRepoUrl())) {
			new TransformationScriptDownloader().initScriptDownloader(gitRepo).removeGitFiles();
		}

		harvestersManager.remove(harvester);
		return "/user/dashboard?faces-redirect=true";
	}

	@Transactional
	public void addFilter() {
		harvester.getFilters().add(new Filter());
		int idx = 0;
		for (Filter filter : harvester.getFilters()) {
			filter.setIndex(idx++);
		}
	}

	@Transactional
	public void removeFilter(Filter filter) {
		harvester.getFilters().remove(filter);
		int idx = 0;
		for (Filter f: harvester.getFilters()) {
			f.setIndex(idx++);
		}
	}

	public void start() {
		start(harvester);
	}

	@Transactional
	public void start(Harvester harvester) {
		Date next = quartzSchedulerService.scheduleJob(harvester.getId().toString(), Frequency.manually, new Date());
		if (next != null) {
			harvester.setNextHarvestDate(next);
//			harvestersManager.update(harvester);
		}
	}

	public void stop() {
		stop(harvester);
	}

	public void stop(Harvester harvester) {
		quartzSchedulerService.interruptJob(harvester.getId().toString() );
	}

	@Transactional
	public List<Run> getMostRecentRuns() {
		harvester = harvestersManager.find(harvester.getId());
		Set<Run> runs = harvester.getRuns();
		return runs.stream().limit(5).collect(Collectors.toList());
	}

	@Transactional
	public List<Log> getMostRecentLogs() {
		harvester = harvestersManager.find(harvester.getId());
		if (harvester.getRuns().isEmpty()){
			return Collections.emptyList();
		} else {
			Set<Run> runs = harvester.getRuns();
			return runs.stream().findFirst().get().getLogs().stream().limit(20).collect(Collectors.toList());
		}
	}

	public boolean isRunning() {
		return isRunning(harvester);
	}

	public boolean isRunning(Harvester harvester) {
		return quartzSchedulerService.isRunning(harvester.getId().toString());
	}

	@Transactional
	public void refresh() {
		harvestersManager.refresh(harvester);
	}

	public void downloadHarvesterAsXml() {
		String harvesterXml = new HarvesterXmlHandler().exportHarvester(harvester);
		if (harvesterXml != null) {
			try (InputStream inputStream = new ByteArrayInputStream(harvesterXml.getBytes(StandardCharsets.UTF_8))) {

				byte[] buf = new byte[harvesterXml.getBytes("UTF-8").length];
				int numRead, offset = 0;

				while ((offset < buf.length) && ((numRead = inputStream.read(buf, offset, buf.length - offset)) >= 0)) {
					offset += numRead;
				}

				HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
				response.setContentType("application/octet-stream");
				response.setHeader("Content-Disposition", "attachment;filename=" + harvester.getName().replace(" ", "_").toLowerCase() + ".xml");
				response.getOutputStream().write(buf);
				response.getOutputStream().flush();
				response.getOutputStream().close();
				FacesContext.getCurrentInstance().responseComplete();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrHarvesterDownloadFailed", null);
		}
	}


	/**
	 * Generates a Harvester object from the content of a Part object by utilizing the HarvesterXmlHandler
	 *
	 * @return
	 * 		the URL of the showHarvester
     */
	@Transactional
	public void handleHarvesterFileImport() {
		if (importedHarvesterFile != null) {
			try {
				String harvesterXml = new Scanner(importedHarvesterFile.getInputStream()).useDelimiter("\\A").next();
				Harvester harvester = new HarvesterXmlHandler().importHarvester(harvesterXml);

				if (harvester != null) {
					//Prevent name collisions
					if (harvestersManager.nameExists(harvester.getName())) harvester.setName(harvester.getName() + "-" + System.currentTimeMillis());

					//Persist repositories separately to prevent transient object exception
					handleImportedRepositories(harvester.getSource());
					handleImportedRepositories(harvester.getTarget());

					//set missing meta data
					harvester.setOwner(identity.getAccount().getId());
					harvester.setVisibility(Visibility.private_visibility);
					harvester.setFrequency(Frequency.manually);

					harvestersManager.persist(harvester);

					//Reload page after successful upload
					FacesContext.getCurrentInstance().getExternalContext().redirect("/MetadataTransformerService/public/harvesters.xhtml");
				} else {
					AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrNameExists", null);
				}
			} catch (IOException e) {
				log.error("InputStream failed: " + e.getMessage());
			}
		} else {
			AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrNoFileSpecified", null);
		}
		AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrGeneral", null);
	}

	@Transactional
	private void handleImportedRepositories(Repository repository) {
		if (repository != null) {
			if (repositoriesManager.nameExists(repository.getName()))
				repository.setName(repository.getName() + "-" + System.currentTimeMillis());

			repository.setOwner(identity.getAccount().getId());
			repositoriesManager.persist(repository);
		}
	}

	public void validateHarvesterFile(FacesContext context, UIComponent component, Object file) throws ValidationException {
		if (file != null) {
			Part harvesterFile = (Part) file;
			if (!"text/xml".equals(harvesterFile.getContentType())) {
				throw new ValidationException("Not a valid XML file.");
			}			
		}
	}

	public Part getImportedHarvesterFile() {
		return importedHarvesterFile;
	}

	public void setImportedHarvesterFile(Part importedHarvesterFile) {
		this.importedHarvesterFile = importedHarvesterFile;
	}

	//Returns a list of all script files stored in the local git directory
	public List<Path> listAllScripts() {
		return gitRepo != null ? new TransformationScriptDownloader().initScriptDownloader(gitRepo).listAllScripts() : Collections.emptyList();
	}

	public String getReadableFilePath(Path gitFile) {
		Path gitRepoDir = gitRepo.getRepoDirWithBranch();
		return gitRepoDir != null ? gitRepoDir.relativize(gitFile).toString() : gitFile.getFileName().toString();
	}

	public void fetchGitRepository() {
		if (gitRepo == null) {
			gitRepo = new TransformationScriptDownloader.GitRepository(
					harvester.getGitRepoUrl(),
					harvester.getGitBranch(),
					harvester.getGitFileName(),
					harvester.isGitRepoRequiresCredentials(),
					harvester.isGitRepoSshAuthentication(),
					harvester.getGitUserName(),
					harvester.getGitPassWord());
		}
		new TransformationScriptDownloader().initScriptDownloader(gitRepo).updateOrGetRepo();
	}

	@Produces
	@Named
	public Harvester getHarvester() {
		return harvester;
	}

	@Produces
	@Named
	public TransformationScriptDownloader.GitRepository getGitRepo() {
		return gitRepo;
	}

    @Produces
    @Named
    public String getScript() {
        return script;
    }

    @Transactional
	public List<Filter> getFilters() {
		if (id != null) {
			harvester = harvestersManager.find(id);
		}
		return new ArrayList<>(harvester.getFilters());
	}

	public HarvesterEnums.TransformationScriptSource[] getTransformationScriptSources() {
		return HarvesterEnums.TransformationScriptSource.values();
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

}
