package de.fhg.fokus.odp.harvester.actions;

import de.fhg.fokus.odp.harvester.application.AppLocalization;
import de.fhg.fokus.odp.harvester.persistence.HarvestersManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.entities.Repository;
import de.fhg.fokus.odp.harvester.persistence.enums.Frequency;
import de.fhg.fokus.odp.harvester.persistence.enums.RepositoryType;
import de.fhg.fokus.odp.harvester.rdf.CountriesAuthorityTable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Named
@ViewScoped
public class HarvesterController implements Serializable {

	private static final long serialVersionUID = -534693913871978425L;
	
	private String searchTerm = "";
	private boolean errorsOnly = false;

	private final Integer PAGESIZE = 15;

	private Integer offset = 0;

	private int sort = 1;

	@Inject
	private AppLocalization localization;

	@Inject
	private CountriesAuthorityTable mdrCountries;

	@Inject
	private HarvestersManager harvestersManager;

	private List<Harvester> harvesters;

	private List<Frequency> selectedFrequencies = new ArrayList<>();
	private Map<Frequency, Long> frequencyCounters = new HashMap<>();

	private List<RepositoryType> selectedTypes = new ArrayList<>();
	private Map<RepositoryType, Long> typeCounters = new HashMap<>();

	private List<String> selectedCountries = new ArrayList<>();
	private Map<String, Long> countryCounters = new HashMap<>();

	@Transactional
	public void load() {
		fetchlist();
	}

	@Transactional
	public void search() {
		fetchlist();
	}

	private void fetchlist() {
		offset = 0;
		harvesters = harvestersManager.listHarvesters(null, null, searchTerm, errorsOnly);
		frequencyCounters = harvesters.stream().collect(Collectors.groupingBy(Harvester::getFrequency, Collectors.counting()));
		typeCounters = harvesters.stream().map(Harvester::getSource).collect(Collectors.groupingBy(Repository::getRepositoryType, Collectors.counting()));
		countryCounters = harvesters.stream().map(Harvester::getSource).flatMap(r -> r.getSpatial().stream()).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset < 0 ? 0 : offset;
	}

	public Long getTotal() {
		return getHarvestersStream().count();
	}

	public Long getRows() {
		long size = getTotal();
		return size - PAGESIZE > offset ? PAGESIZE : size - offset;
	}

	public List<Harvester> getAllHarvesters() {
		if (sort == 1 || sort == 2) {
			harvesters.sort(Comparator.comparing(Harvester::getName));
		} else {
			harvesters.sort(Comparator.nullsLast(Comparator.comparing(Harvester::getNextHarvestDate)));
		}
		if (sort == 2 || sort == 4) {
			Collections.reverse(harvesters);
		}
		return harvesters;
	}

	public List<Harvester> getHarvesters() {
		return getHarvestersStream().skip(offset).limit(PAGESIZE).collect(Collectors.toList());
	}

	private Stream<Harvester> getHarvestersStream() {
		Stream<Harvester> stream = harvesters.stream();
		if (!selectedFrequencies.isEmpty()) {
			stream = stream.filter(h -> selectedFrequencies.contains(h.getFrequency()));
		}
		if (!selectedTypes.isEmpty()) {
			stream = stream.filter(h -> selectedTypes.contains(h.getSource().getRepositoryType()));
		}
		if (!selectedCountries.isEmpty()) {
			stream = stream.filter(h -> h.getSource().getSpatial().stream().map(s -> mdrCountries.prefLabel(s, localization.getLocaleCode())).anyMatch(selectedCountries::contains));
		}
		return stream;
	}

	public Frequency[] getFrequencies() {
		return Frequency.values();
	}

	public RepositoryType[] getRepositoryTypes() {
		return RepositoryType.values();
	}

	public String[] getCountries() {
		return mdrCountries.getEurope(localization.getLocaleCode()).keySet().toArray(new String[] {});
	}

	public String getSearchTerm() {
		return searchTerm;
	}
	
	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	public boolean isErrorsOnly() {
		return errorsOnly;
	}

	public void setErrorsOnly(boolean errorsOnly) {
		this.errorsOnly = errorsOnly;
	}

	@Transactional
	public void nextPage() {
		offset += PAGESIZE;
	}

	@Transactional
	public void previousPage() {
		offset -= PAGESIZE;
	}

	public void toggleFrequency(Frequency frequency) {
		if (selectedFrequencies.contains(frequency)) {
			selectedFrequencies.remove(frequency);
		} else {
			selectedFrequencies.add(frequency);
		}
		offset = 0;
	}

	public boolean isFrequencySelected(Frequency frequency) {
		return selectedFrequencies.contains(frequency);
	}

	public void toggleSourceType(RepositoryType type) {
		if (selectedTypes.contains(type)) {
			selectedTypes.remove(type);
		} else {
			selectedTypes.add(type);
		}
		offset = 0;
	}

	public boolean isTypeSelected(RepositoryType type) {
		return selectedTypes.contains(type);
	}

	public void toggleCountry(String country) {
		if (selectedCountries.contains(country)) {
			selectedCountries.remove(country);
		} else {
			selectedCountries.add(country);
		}
		offset = 0;
	}

	public boolean isCountrySelected(String country) {
		return selectedCountries.contains(country);
	}

	public Long getFrequencyCounter(Frequency frequency) {
		return frequencyCounters.containsKey(frequency) ? frequencyCounters.get(frequency) : Long.valueOf(0);
	}

	public Long getTypeCounter(RepositoryType type) {
		return typeCounters.containsKey(type) ? typeCounters.get(type) : Long.valueOf(0);
	}

	public Long getCountryCounter(String country) {
		String ref = mdrCountries.getEurope(localization.getLocaleCode()).get(country);
		return countryCounters.containsKey(ref) ? countryCounters.get(ref) : Long.valueOf(0);
	}

	public void sortAlphabetically() {
		sort = sort == 1 ? 2 : 1;
	}

	public void sortSchedule() {
		sort = sort == 3 ? 4 : 3;
	}

	public int getSort() {
		return sort;
	}

}
