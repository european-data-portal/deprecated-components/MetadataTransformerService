package de.fhg.fokus.odp.harvester.actions;

import de.fhg.fokus.odp.harvester.persistence.RunsManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.entities.Log;
import de.fhg.fokus.odp.harvester.persistence.entities.Run;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.service.scheduler.QuartzSchedulerService;

import javax.enterprise.inject.Produces;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.apache.commons.lang3.time.DurationFormatUtils;

import java.io.Serializable;
import java.time.Duration;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Named
@ViewScoped
public class RunAction implements Serializable {

	private static final long serialVersionUID = 1077368343033780994L;

	@Inject
	private QuartzSchedulerService quartzSchedulerService;

	@Inject
	private RunsManager runsManager;

	private Run run;

	private Integer id;

	private int progress;
	private long infos;
	private long warnings;
	private long errors;

	private List<Log> newestLogs = new ArrayList<>();

	private Duration duration;

	@Produces
	@Named
	public Run getRun() {
		return run;
	}

	@Transactional
	public void load() {
		if (id != null) {
			run = runsManager.find(id);
			initValues();
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Transactional
	public String remove() {
		run.getHarvester().getRuns().remove(run);
		runsManager.remove(run);
		return "/public/showharvester?faces-redirect=true&id=" + run.getHarvester().getId();
	}

	public String stop() {
		quartzSchedulerService.interruptJob(run.getHarvester().getId().toString());
		return "/public/showharvester?faces-redirect=true&id=" + run.getHarvester().getId();
	}

	public int getProgress() {
		return progress;
	}

	public int getInfos() {
		return (int) infos;
	}

	public int getWarnings() {
		return (int) warnings;
	}

	public int getErrors() {
		return (int) errors;
	}

	@Transactional
	public void refresh() {
		run = runsManager.find(run.getId());
		initValues();
	}

	public String getDuration() {
		return DurationFormatUtils.formatDuration(duration.toMillis(), "HH:mm:ss");
	}

	public List<Log> getNewestLogs() {
		return newestLogs;
	}

	private void initValues() {
		int processing = run.getNumberToProcess();
		progress = processing == 0 ? 0 : ((run.getNumberAdded() + run.getNumberUpdated() + run.getNumberRejected() + run.getNumberSkipped()) * 100) / processing;

		Map<LogEnums.Severity, Long> counters = run.getLogs().stream().collect(Collectors.groupingBy(Log::getSeverity, Collectors.counting()));
		infos = counters.getOrDefault(LogEnums.Severity.info, 0L);
		warnings = counters.getOrDefault(LogEnums.Severity.warning, 0L);
		errors = counters.getOrDefault(LogEnums.Severity.error, 0L);

		newestLogs = run.getLogs().stream().limit(20).collect(Collectors.toList());

		if (run == null || run.getStartTime() == null || run.getEndTime() == null) {
			duration = Duration.ZERO;
		} else {
			duration = Duration.between(run.getStartTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(), run.getEndTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
		}
	}

}
