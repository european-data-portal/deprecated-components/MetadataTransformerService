package de.fhg.fokus.odp.harvester.actions;

import de.fhg.fokus.odp.harvester.persistence.HarvestersManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.entities.Log;
import de.fhg.fokus.odp.harvester.persistence.entities.Run;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.RunState;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Named
@ViewScoped
public class RunController implements Serializable {

    private static final long serialVersionUID = 456L;

    @Inject
    private HarvestersManager harvestersManager;

    private Harvester harvester;

    private List<Run> runs;

    private List<RunState> selectedRunStates;

    private boolean groupByRunState;

    private Integer id;

    private final Integer PAGESIZE = 15;

    private Integer offset;

    private Integer total;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getRows() {
        return total - PAGESIZE > offset ? PAGESIZE : total - offset;
    }

    public Integer getTotal() {
        return total;
    }

    public Integer getPageSize() {
        return PAGESIZE;
    }

    @Transactional
    public void load() {
        if (offset == null) {
            offset = 0;
        }

        if (selectedRunStates == null) {
            selectedRunStates = new ArrayList<>(Arrays.asList(getRunStates()));
        }
        groupByRunState = false;

        refresh();
    }

    public Harvester getHarvester() {
    	return harvester;
    }
    
    public List<Integer> chartTest() {
        List<Integer> list = new ArrayList<>();
        Random r = new Random();
        for (int i = 2000; i < 2010; i++) {
            list.add(r.nextInt(500) + 800);
        }
        return list;
    }

    @Transactional
	public void cleanRuns() {
	    harvester = harvestersManager.find(harvester.getId());
		harvester.getRuns().clear();

		offset = 0;
		refresh();
	}
	
    //Only returns completed runs, i.e. with status finished, interrupted, or error
    public List<Run> getRunsBySelectedStates() {
        return runs;
    }

    public int getCountCompletedRuns() {
        return (int) harvester.getRuns().stream().filter(r -> r.getRunState() == RunState.finished).count();
    }

    public int getCountRunsWithOutErrors() {
        return (int) harvester.getRuns().stream().filter(r -> r.getRunState() != RunState.error).count();
    }

    public int getCountRunsWithErrors() {
        return (int) harvester.getRuns().stream().filter(r -> r.getRunState() == RunState.error).count();
    }

    public Long getMeanRunDuration() {
        Double meanDuration = harvester.getRuns().stream().collect(Collectors.averagingLong(Run::getRunDurationInMinutes));
        return meanDuration.longValue();
    }

    public boolean isSuccessfulRuns() {
        return selectedRunStates.contains(RunState.finished);
    }

    @Transactional
    public void setSuccessfulRuns(boolean isSuccess) {
        if (!isSuccess) {
            selectedRunStates.remove(RunState.finished);
        } else {
            if (!selectedRunStates.contains(RunState.finished)) {
                selectedRunStates.add(RunState.finished);
            }
        }
        refresh();
    }

    public boolean isInterruptedRuns() {
        return selectedRunStates.contains(RunState.interrupted);
    }

    @Transactional
    public void setInterruptedRuns(boolean isInterrupted) {
        if (!isInterrupted) {
            selectedRunStates.remove(RunState.interrupted);
        } else {
            if (!selectedRunStates.contains(RunState.interrupted)) {
                selectedRunStates.add(RunState.interrupted);
            }
        }
        refresh();
    }

    public boolean isFailedRuns() {
        return selectedRunStates.contains(RunState.error);
    }

    @Transactional
    public void setFailedRuns(boolean isFailed) {
        if (!isFailed) {
            selectedRunStates.remove(RunState.error);
        } else {
            if (!selectedRunStates.contains(RunState.error)) {
                selectedRunStates.add(RunState.error);
            }
        }
        refresh();
    }

    public boolean isGroupByRunState() {
        return groupByRunState;
    }

    @Transactional
    public void setGroupByRunState(boolean groupByRunState) {
        this.groupByRunState = groupByRunState;
        refresh();
    }

    public RunState[] getRunStates() {
        return new RunState[] {RunState.finished, RunState.interrupted, RunState.error};
    }

    @Transactional
    public LogEnums.Category getMostCommonError() {
        harvester = harvestersManager.find(id);
        Map<LogEnums.Category, Long> categories = harvester.getRuns().stream().map(Run::getLogs).flatMap(List<Log>::stream).collect(Collectors.groupingBy(Log::getCategory, Collectors.counting()));
        Optional<Map.Entry<LogEnums.Category, Long>> category = categories.entrySet().stream().max(Map.Entry.comparingByValue());
        return category.isPresent() ? category.get().getKey() : null;
    }

    @Transactional
    public void nextPage() {
        offset += PAGESIZE;
        refresh();
    }

    @Transactional
    public void previousPage() {
        offset -= PAGESIZE;
        refresh();
    }

    public void refresh() {
        harvester = harvestersManager.find(id);
        runs = harvester.getRuns().stream().filter(r -> selectedRunStates.contains(r.getRunState())).skip(offset).limit(PAGESIZE).sorted(Comparator.comparing(Run::getRunState)).collect(Collectors.toList());
        total = (int) harvester.getRuns().stream().filter(r -> selectedRunStates.contains(r.getRunState())).count();
    }

}
