package de.fhg.fokus.odp.harvester.actions;

import de.fhg.fokus.odp.harvester.application.AppLocalization;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.RelationshipManager;
import org.picketlink.idm.model.basic.BasicModel;
import org.picketlink.idm.model.basic.Grant;
import org.picketlink.idm.model.basic.Role;
import org.picketlink.idm.model.basic.User;
import org.picketlink.idm.query.IdentityQuery;

import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Named
@ViewScoped
public class UserController implements Serializable {

    @Inject
    private IdentityManager identityManager;

    @Inject
    private RelationshipManager relationshipManager;

    private List<User> users;

    private Role selectedRole;

    @Transactional
    public void load() {
        IdentityQuery<User> query = identityManager.getQueryBuilder().createIdentityQuery(User.class);
        users = query.getResultList();
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Role> getRoles(User user) {
        List<Grant> grants = relationshipManager.createRelationshipQuery(Grant.class).setParameter(Grant.ASSIGNEE, user).getResultList();
        return grants.stream().map(Grant::getRole).collect(Collectors.toList());
    }

    public List<Role> getRoles() {
        List<Grant> grants = relationshipManager.createRelationshipQuery(Grant.class).getResultList();
        return grants.stream().map(Grant::getRole).collect(Collectors.toList());
    }

    @Transactional
    public void deleteUser(User user) {
        Role adminRole = BasicModel.getRole(identityManager, "Administrator");
        if (BasicModel.hasRole(relationshipManager, user, adminRole)) {
            List<Grant> grants = relationshipManager.createRelationshipQuery(Grant.class).setParameter(Grant.ROLE, adminRole).getResultList();
            if (grants.size() <= 1) {
                AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrLastAdmin", null);
                return;
            }
        }
        identityManager.remove(user);
        IdentityQuery<User> query = identityManager.getQueryBuilder().createIdentityQuery(User.class);
        users = query.getResultList();
    }

    public Role getRole() {
        return selectedRole;
    }

    public void setRole(Role role) {
        selectedRole = role;
    }

}
