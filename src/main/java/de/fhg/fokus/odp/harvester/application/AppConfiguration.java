package de.fhg.fokus.odp.harvester.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import java.io.IOException;
import java.util.Properties;

@ApplicationScoped
public class AppConfiguration {
    private final String TRANSFORMER_PROPERTIES_FILENAME = "transformer.properties";

    public static final String TRANSFORMER_INSPIRE_ADAPTER = "transformer.inspire.adapter";
    public static final String TRANSFORMER_INSTANCE = "transformer.instance";
    public static final String TRANSFORMER_QUEUES_CAPACITY = "transformer.queues.capacity";
    private static final String TRANSFRORMER_SERVICE_FILE_LIST = "transformer.factory.files";

    private final Logger log = LoggerFactory.getLogger(getClass());

    private Properties props = new Properties();

    public void init(@Observes @Initialized(ApplicationScoped.class) Object init) {
        try {
            props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(TRANSFORMER_PROPERTIES_FILENAME));
            StringBuffer buffer = new StringBuffer();
            props.forEach((key, value) -> {
                buffer.append("  ");
                buffer.append(key.toString());
                buffer.append(" = ");
                buffer.append(value.toString());
                buffer.append("\n");
            });
            log.info("using configuration:\n{}", buffer.toString());
        } catch (IOException e) {
            log.error("loading configuration", e);
        }
    }

    private String getenv(String env) {
        return System.getenv(env.toUpperCase().replaceAll("\\.", "_"));
    }

    public String get(String property) {
        String env = getenv(property);
        return env != null ? env : props.getProperty(property);
    }

    public String get(String property, String defaultValue) {
        String env = getenv(property);
        return env != null ? env : props.getProperty(property, defaultValue);
    }

    public String getInstance() {
        String env = getenv(TRANSFORMER_INSTANCE);
        return env != null ? env : props.getProperty(TRANSFORMER_INSTANCE, System.getProperty("jboss.qualified.host.name"));
    }

    public int getQueuesCapacity() {
        String env = getenv(TRANSFORMER_QUEUES_CAPACITY);
        return Integer.parseInt(env != null ? env : props.getProperty(TRANSFORMER_QUEUES_CAPACITY, "0"));
    }

    public String getInspireAdapter() {
        String env = getenv(TRANSFORMER_INSPIRE_ADAPTER);
        return env != null ? env : props.getProperty(TRANSFORMER_INSPIRE_ADAPTER, null);
    }

    public String getFileList() {
        String env = getenv(TRANSFRORMER_SERVICE_FILE_LIST);
        return env != null ? env : props.getProperty(TRANSFRORMER_SERVICE_FILE_LIST, "");
    }

}
