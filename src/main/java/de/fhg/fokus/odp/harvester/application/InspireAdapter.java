package de.fhg.fokus.odp.harvester.application;

import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.http.HttpConnector;
import org.apache.http.client.config.RequestConfig;
import org.jdom2.Document;
import org.jdom2.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@ApplicationScoped
public class InspireAdapter {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private String adapterUrl;

    private HttpConnector<Document> client;

    private List<InspireHarvesterInfo> harvesters = new ArrayList<>();

    @Inject
    private AppConfiguration appConfiguration;

    @PostConstruct
    public void init() {
        adapterUrl = appConfiguration.getInspireAdapter();
        if (adapterUrl != null) {
            client = HttpConnector.createConnector(adapterUrl, null, Document.class);
        }
    }

    public void fetchHarvesters() {
        if (adapterUrl != null) {
            try {
                final RequestConfig params = RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(1000).build();
                Document response = client.get("/mgmt/harvesters", params);
                harvesters = response.getRootElement().getChildren("harvester").stream().map(this::convertToJson).collect(Collectors.toList());
            } catch (IOException | ProtocolException e) {
                log.error("fetching harvesters", e);
            }
        }
    }

    private InspireHarvesterInfo convertToJson(Element harvester) {
        InspireHarvesterInfo info = new InspireHarvesterInfo();
        info.setId(harvester.getChildText("id"));
        info.setType(harvester.getChildText("type"));
        info.setName(harvester.getChildText("name"));
        info.setDescription(harvester.getChildText("description"));
        info.setEndpoint(harvester.getChildText("endpoint"));
        info.setUrl(harvester.getChildText("url"));
        info.setSelective(Boolean.valueOf(harvester.getChildText("selective")));

        return info;
    }

    public List<InspireHarvesterInfo> getHarvesters() {
        return harvesters;
    }

}
