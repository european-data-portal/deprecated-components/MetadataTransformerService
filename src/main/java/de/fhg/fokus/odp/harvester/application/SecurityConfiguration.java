package de.fhg.fokus.odp.harvester.application;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.picketlink.config.SecurityConfigurationBuilder;
import org.picketlink.event.PartitionManagerCreateEvent;
import org.picketlink.event.SecurityConfigurationEvent;
import org.picketlink.idm.PartitionManager;
import org.picketlink.idm.config.IdentityConfiguration;
import org.picketlink.idm.config.IdentityConfigurationBuilder;
import org.picketlink.idm.model.basic.Realm;

@ApplicationScoped
public class SecurityConfiguration {

	@Produces
	@PersistenceContext
	private EntityManager picketLinkEntityManager;

	@Produces
	IdentityConfiguration produceIdentityManagementConfiguration() {
		IdentityConfigurationBuilder builder = new IdentityConfigurationBuilder();

		builder.named("default").stores().jpa().supportAllFeatures();

		return builder.build();
	}

	@Transactional
	public void initPartitionManager(@Observes PartitionManagerCreateEvent event) {
		// retrieve the recently created partition manager instance
		PartitionManager partitionManager = event.getPartitionManager();		
		if (partitionManager.getPartition(Realm.class, Realm.DEFAULT_REALM) == null) {
			partitionManager.add(new Realm(Realm.DEFAULT_REALM));			
		}
	}

	public void configureHttpSecurity(@Observes SecurityConfigurationEvent event) {
		SecurityConfigurationBuilder builder = event.getBuilder();

		builder.http()
			.forPath("/user/*").authenticateWith()
				.form().authenticationUri("/user/login.xhtml")
					.loginPage("/user/login.xhtml")
					.errorPage("/user/error.xhtml")
					.restoreOriginalRequest()
			.forPath("/protected/*")
				.authorizeWith().role("Administrator")
//			.forPath("/rest/*")
//				.authenticateWith().basic()
//				.authorizeWith().role("Administrator")
			.forPath("/logout")
				.logout()
				.redirectTo("/index.html");
	}

}
