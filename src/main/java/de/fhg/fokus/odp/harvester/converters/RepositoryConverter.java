package de.fhg.fokus.odp.harvester.converters;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import de.fhg.fokus.odp.harvester.persistence.RepositoriesManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Repository;

@Named
@ApplicationScoped
public class RepositoryConverter implements Converter {

	@Inject
	private RepositoriesManager repositoriesManager;
	
	@Override
	@Transactional
	public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
		return repositoriesManager.find(Integer.valueOf(value));
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		if (value instanceof String) {
			return value.toString();
		}
		return ((Repository)value).getId().toString();
	}

}
