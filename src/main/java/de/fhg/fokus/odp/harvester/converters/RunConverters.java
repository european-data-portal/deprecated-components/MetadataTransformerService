package de.fhg.fokus.odp.harvester.converters;

import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.RunState;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;

public class RunConverters {

    @Named
    @ApplicationScoped
    public static class RunStateConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
            return RunState.valueOf(s.toLowerCase().replaceAll(" ", "_"));
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
            if (o == null) {
                return null;
            }
            if (o instanceof String) {
                return o.toString();
            }
            return ((RunState) o).getLabel();
        }
    }

    @Named
    @ApplicationScoped
    public static class LogSeverityConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
            return LogEnums.Severity.valueOf(s.toLowerCase());
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
            if (o == null) {
                return null;
            }
            if (o instanceof String) {
                return o.toString();
            }
            return ((LogEnums.Severity) o).getLabel();
        }
    }

    @Named
    @ApplicationScoped
    public static class LogCategoryConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
            return LogEnums.Category.valueOf(s.toLowerCase());
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
            if (o == null) {
                return null;
            }
            if (o instanceof String) {
                return o.toString();
            }
            return ((LogEnums.Category) o).getLabel();
        }
    }
}
