package de.fhg.fokus.odp.harvester.exceptions;


public class NameExistsException extends ProtocolException {
    /**
     * Serialization version.
     */
    private static final long serialVersionUID = 1L;

    public NameExistsException() {
    }

    public NameExistsException(String message) {
        super(message);
    }

    public NameExistsException(Throwable cause) {
        super(cause);
    }

    public NameExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
