package de.fhg.fokus.odp.harvester.exceptions;

/**
 * Created by jpi on 15.04.2015.
 */
public class ProtocolException extends Exception {

	public ProtocolException() {}

    public ProtocolException(String message) {
        super(message);
    }

    public ProtocolException(Throwable throwable) {
        super(throwable);
    }


    public ProtocolException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
