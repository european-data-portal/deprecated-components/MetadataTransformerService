package de.fhg.fokus.odp.harvester.exceptions;

public class ValidationException extends ProtocolException {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1246380683531694733L;
	
	private String message = "Validation failed.";

    public ValidationException() {
        super();    
    }
    
      public ValidationException(String message) {
        super(message);
        this.message = message;
    }
    
    public ValidationException(Throwable throwable) {
        super(throwable);
        message = throwable.getMessage();
    }

  
    public ValidationException(String message, Throwable throwable) {
        super(message, throwable);
        this.message = message;
    }
    
  
    public String getMessage() {
        return message;
    }
    
   
    public void setMessage(String message) {
        this.message = message;
    }

	
}
