package de.fhg.fokus.odp.harvester.exceptions.oaipmh;

import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;

public class BadVerbException extends ProtocolException {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 5492498791973301613L;
	
	private String message = "Value of the verb argument is not a legal OAI-PMH verb, the verb argument is missing, or the verb argument is repeated.";

    public BadVerbException() {
        super();    
    }
    
      public BadVerbException(String message) {
        super(message);
        this.message = message;
    }
    
    public BadVerbException(Throwable throwable) {
        super(throwable);
        message = throwable.getMessage();
    }

  
    public BadVerbException(String message, Throwable throwable) {
        super(message, throwable);
        this.message = message;
    }
    
  
    public String getMessage() {
        return message;
    }
    
   
    public void setMessage(String message) {
        this.message = message;
    }

	
}
