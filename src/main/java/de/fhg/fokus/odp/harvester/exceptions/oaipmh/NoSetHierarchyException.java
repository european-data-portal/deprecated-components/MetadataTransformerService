package de.fhg.fokus.odp.harvester.exceptions.oaipmh;

import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;

public class NoSetHierarchyException extends ProtocolException {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 5529917358681302049L;
	
	private String message = "The repository does not support sets.";

    public NoSetHierarchyException() {
        super();    
    }
    
      public NoSetHierarchyException(String message) {
        super(message);
        this.message = message;
    }
    
    public NoSetHierarchyException(Throwable throwable) {
        super(throwable);
        message = throwable.getMessage();
    }

  
    public NoSetHierarchyException(String message, Throwable throwable) {
        super(message, throwable);
        this.message = message;
    }
    
  
    public String getMessage() {
        return message;
    }
    
   
    public void setMessage(String message) {
        this.message = message;
    }

	
}
