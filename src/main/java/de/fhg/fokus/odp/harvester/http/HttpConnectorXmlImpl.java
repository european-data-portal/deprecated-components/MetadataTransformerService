package de.fhg.fokus.odp.harvester.http;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import org.apache.http.Header;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class HttpConnectorXmlImpl extends HttpConnector<Document> {

	protected HttpConnectorXmlImpl(String host, Header[] headers) {
		super(host, headers);
	}

	@Override
	protected Document readContent(InputStream content, Charset charset) {
		SAXBuilder builder = new SAXBuilder();
		builder.setIgnoringElementContentWhitespace(true);
		builder.setIgnoringBoundaryWhitespace(true);
		try {
			return builder.build(content);
		} catch (JDOMException | IOException e) {
			log.error("parsing stream", e);
		}
		return null;		
	}
	
}
