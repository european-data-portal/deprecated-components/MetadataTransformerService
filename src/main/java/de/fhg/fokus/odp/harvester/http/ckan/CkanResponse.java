package de.fhg.fokus.odp.harvester.http.ckan;

import com.fasterxml.jackson.databind.JsonNode;

import de.fhg.fokus.odp.harvester.http.HttpResponse;

public class CkanResponse extends HttpResponse<JsonNode> {

	public CkanResponse(JsonNode content) {
		super(content);
	}

	@Override
	public boolean isError() {
		return content != null && !content.path("success").asBoolean();
	}

	@Override
	public boolean isSuccess() {
		return content != null && content.path("success").asBoolean();
	}
	
	public String getHelp() {
		return content == null ? "" : content.path("help").textValue();
	}

	@Override
	public CkanError getError() {
		return !isError() ? null : new CkanError(content.path("error"));
	}

	@Override
	public CkanResult getResult() {
		return isError() ? null : new CkanResult(content.path("result"));
	}

}
