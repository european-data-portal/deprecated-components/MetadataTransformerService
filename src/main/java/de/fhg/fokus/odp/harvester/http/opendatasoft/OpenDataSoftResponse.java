package de.fhg.fokus.odp.harvester.http.opendatasoft;

import com.fasterxml.jackson.databind.JsonNode;

import de.fhg.fokus.odp.harvester.http.HttpResponse;

public class OpenDataSoftResponse extends HttpResponse<JsonNode> {

    public OpenDataSoftResponse(JsonNode content) {
        super(content);
    }

    @Override
    public boolean isError() {
        return content == null || content.path("datasets").isMissingNode();
    }

    @Override
    public boolean isSuccess() {
        return !isError();
    }

    @Override
    public OpenDataSoftResult getResult() {
        return isSuccess() ? new OpenDataSoftResult(content) : null;
    }

    @Override
    public OpenDataSoftError getError() {
        return isError() ? new OpenDataSoftError(content) : null;
    }
    
}

