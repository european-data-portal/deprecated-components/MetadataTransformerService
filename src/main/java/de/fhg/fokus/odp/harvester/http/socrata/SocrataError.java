package de.fhg.fokus.odp.harvester.http.socrata;


import com.fasterxml.jackson.databind.JsonNode;
import de.fhg.fokus.odp.harvester.http.HttpError;

public class SocrataError extends HttpError<JsonNode> {

    public SocrataError(JsonNode error) {
        super(error);
    }

    @Override
    public String getType() {
        return error.path("code").textValue();
    }

    @Override
    public String getMessage() {
        return error.path("message").textValue();
    }
}