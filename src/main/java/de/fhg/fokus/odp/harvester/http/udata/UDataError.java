package de.fhg.fokus.odp.harvester.http.udata;

import com.fasterxml.jackson.databind.JsonNode;

import de.fhg.fokus.odp.harvester.http.HttpError;

public class UDataError extends HttpError<JsonNode> {

	public UDataError(JsonNode error) {
		super(error);
	}

	@Override
	public String getType() {
		return error.path("__type").textValue();		
	}

	@Override
	public String getMessage() {
		return error.path("message").textValue();
	}
	
}
