package de.fhg.fokus.odp.harvester.http.udata;

import com.fasterxml.jackson.databind.JsonNode;

import de.fhg.fokus.odp.harvester.http.HttpResult;

public class UDataResult extends HttpResult<JsonNode> {

	public UDataResult(JsonNode result) {
		super(result);
	}

	@Override
	public JsonNode getContent() {
		return result;
	}
	
}
