package de.fhg.fokus.odp.harvester.persistence;

import de.fhg.fokus.odp.harvester.persistence.entities.Dataset;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

/**
 * Created by sim on 10.01.2017.
 */
@ApplicationScoped
public class DatasetsManager {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Dataset find(String id) {
        return em.find(Dataset.class, id);
    }

    @Transactional
    public void persist(Dataset dataset) {
        em.persist(dataset);
    }

    @Transactional
    public void remove(Dataset dataset) {
        em.remove(em.contains(dataset) ? dataset : em.merge(dataset));
    }

    @Transactional
    public Dataset update(Dataset dataset) {
        return em.merge(dataset);
    }

    @Transactional
    public void refresh(Dataset dataset) {
        em.refresh(em.contains(dataset) ? dataset : em.merge(dataset));
    }

    @Transactional
    public List<Dataset> findByNameAndCatalog(String name, Integer catalog) {
        return em.createQuery("select d from Dataset d where d.name = :name and d.catalog = :catalog order by d.created", Dataset.class)
                .setParameter("name", name)
                .setParameter("catalog", catalog)
                .getResultList();
    }

}
