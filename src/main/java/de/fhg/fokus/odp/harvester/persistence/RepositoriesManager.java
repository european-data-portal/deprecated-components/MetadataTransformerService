package de.fhg.fokus.odp.harvester.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;

import de.fhg.fokus.odp.harvester.persistence.enums.RepositoryType;
import org.picketlink.Identity;

import de.fhg.fokus.odp.harvester.persistence.entities.Repository;
import de.fhg.fokus.odp.harvester.persistence.enums.Visibility;

@ApplicationScoped
public class RepositoriesManager {

	@PersistenceContext
	private EntityManager em;

	@Inject
	private Identity identity;

	public void persist(Repository repository) {
		em.persist(repository);
	}

	public void remove(Repository repository) {
		em.remove(em.contains(repository) ? repository : em.merge(repository));
	}

	public Repository update(Repository repository) {
		return em.merge(repository);
	}

	public Long count() {
		return em.createQuery("select count(r) from Repository r", Long.class).getSingleResult();
	}

	public List<Repository> list(Integer rows, Integer start) {
		return paging(em.createNamedQuery("repositories", Repository.class), rows, start).getResultList();
	}

	public Repository find(Integer id) {
		return em.find(Repository.class, id);
	}

	public Repository find(String name) {return em.createNamedQuery("repository_by_name", Repository.class).setParameter("name", name).getSingleResult(); }

	@Transactional
	public Long countUserRepositories() {
		return em.createQuery("select count(r) from Repository r where r.owner = :owner", Long.class).setParameter("owner", identity.getAccount().getId()).getSingleResult();
	}

	public String getMostUsedRepository() {
		if (identity.isLoggedIn()) {
			List<?> results = em.createNativeQuery("select repository.name from repository, harvester where repository.owner = ?1 and (repository.id = harvester.source_id or repository.id = harvester.target_id) group by repository.name order by count(repository.name) desc limit 1")
					.setParameter(1, identity.getAccount().getId()).getResultList();
			if (results.size() > 0) {
				return results.get(0).toString();
			} else {
				return "-";
			}
		} else {
			return "-";
		}
	}

	@Transactional
	public List<Repository> listUserRepositories(Integer rows, Integer start) {
		TypedQuery<Repository> query = em.createNamedQuery("repositories_from_owner", Repository.class)
				.setParameter("owner", identity.getAccount().getId());
		return paging(query, rows, start).getResultList();
	}

	@Transactional
	public Long countRepositories(String searchterm, List<RepositoryType> selectedRepositoryTypes) {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Long> critquery = builder.createQuery(Long.class);
		Root<Repository> repository = critquery.from(Repository.class);
		
		List<Predicate> predicates = new ArrayList<>();
		Map<String, Object> params = new HashMap<>();

		ParameterExpression<Visibility> visibilityParam = builder.parameter(Visibility.class, "visibility");
		Predicate visibilityPredicate = builder.equal(repository.<Visibility>get("visibility"), visibilityParam);
		params.put("visibility", Visibility.public_visibility);

		// owner or public, or only public
		if (identity.isLoggedIn()) {
			ParameterExpression<String> ownerParam = builder.parameter(String.class, "owner");
			predicates.add(builder.or(builder.equal(repository.<String>get("owner"), ownerParam), visibilityPredicate));
			params.put("owner", identity.getAccount().getId());
		} else {
			predicates.add(visibilityPredicate);
		}

		// name like search term
		if (!searchterm.isEmpty()) {
			ParameterExpression<String> searchParam = builder.parameter(String.class, "searchterm");
			predicates.add(builder.like(builder.lower(repository.get("name")), searchParam));
			params.put("searchterm", "%" + searchterm.toLowerCase().trim() + "%");
		}

		// merge owner and searchterm criteria
		Predicate ownerAndSearchPredicate = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		/**
		 * Temp output to demonstrate breaking of Java type system.
		 * When a list of repository types is passed to this method via the drop down selection on the repositories.xhtml page,
		 * the List of type RepositoryType actually contains Strings
		 */
		if (selectedRepositoryTypes.size() > 0 && selectedRepositoryTypes.size() != RepositoryType.values().length) {
			Object o = selectedRepositoryTypes.get(0);
			System.out.println("First object in list is of type [" + o.getClass().getCanonicalName()
					+ "] but should be of type RepositoryType");
		}

		// only select specific repository types
		List<Predicate> repositoryTypePredicates = new ArrayList<>();
		List<RepositoryType> convertedTypes = new ArrayList<>();
		for (int i = 0; i < selectedRepositoryTypes.size(); i++) {
			Object rt = selectedRepositoryTypes.get(i);
			if (rt instanceof String) {
				convertedTypes.add(RepositoryType.valueOf((String) rt));
			} else if (rt instanceof RepositoryType) {
				convertedTypes.add((RepositoryType) rt);
			}
		}
		convertedTypes.forEach(type -> repositoryTypePredicates.add(builder.equal(repository.get("repositoryType"), type)));
		Predicate typePredicate = builder.or(repositoryTypePredicates.toArray(new Predicate[repositoryTypePredicates.size()]));

		// join selections
		critquery.where(ownerAndSearchPredicate, typePredicate);

		TypedQuery<Long> query = em.createQuery(critquery.select(builder.count(repository)));
		for (Entry<String, Object> param : params.entrySet()) {
			query = query.setParameter(param.getKey(), param.getValue());
		}
		
		return query.getSingleResult();
	}

	@Transactional
	public List<Repository> listRepositories(Integer rows, Integer start, String searchterm, List<RepositoryType> selectedRepositoryTypes) {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Repository> critquery = builder.createQuery(Repository.class);
		Root<Repository> repository = critquery.from(Repository.class);
		
		List<Predicate> predicates = new ArrayList<>();
		Map<String, Object> params = new HashMap<>();

		//Handle visibility
		ParameterExpression<Visibility> visibilityParam = builder.parameter(Visibility.class, "visibility");
		Predicate visibilityPredicate = builder.equal(repository.<Visibility>get("visibility"), visibilityParam);
		params.put("visibility", Visibility.public_visibility);

		if (identity.isLoggedIn()) {
			ParameterExpression<String> ownerParam = builder.parameter(String.class, "owner");
			predicates.add(builder.or(builder.equal(repository.<String>get("owner"), ownerParam), visibilityPredicate));
			params.put("owner", identity.getAccount().getId());
		} else {
			predicates.add(visibilityPredicate);
		}

		if (!searchterm.isEmpty()) {
			ParameterExpression<String> searchParam = builder.parameter(String.class, "searchterm");
			predicates.add(builder.like(builder.lower(repository.get("name")), searchParam));
			params.put("searchterm", "%" + searchterm.toLowerCase().trim() + "%");
		}

		// merge owner and searchterm criteria
		Predicate ownerAndSearchPredicate = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		// only select specific repository types
		List<Predicate> repositoryTypePredicates = new ArrayList<>();
		List<RepositoryType> convertedTypes = new ArrayList<>();
		for (int i = 0; i < selectedRepositoryTypes.size(); i++) {
			Object rt = selectedRepositoryTypes.get(i);
			if (rt instanceof String) {
				convertedTypes.add(RepositoryType.valueOf((String) rt));
			} else if (rt instanceof RepositoryType) {
				convertedTypes.add((RepositoryType) rt);
			}
		}
		convertedTypes.forEach(type -> repositoryTypePredicates.add(builder.equal(repository.get("repositoryType"), type)));
		Predicate typePredicate = builder.or(repositoryTypePredicates.toArray(new Predicate[repositoryTypePredicates.size()]));

		// join selections
		critquery.where(ownerAndSearchPredicate, typePredicate);
		critquery.orderBy(builder.asc(repository.get("name")));

		TypedQuery<Repository> query = em.createQuery(critquery.select(repository));
		for (Entry<String, Object> param : params.entrySet()) {
			query = query.setParameter(param.getKey(), param.getValue());
		}
		
		return paging(query, rows, start).getResultList();
	}

	public boolean nameExists(String name) {
		return em.createQuery("select count(r) from Repository r where lower(r.name) = :name", Long.class)
				.setParameter("name", name.toLowerCase()).getSingleResult() > 0;
	}

	public boolean isRepositoryInUse(Repository repository) {
		return em.createQuery("select count(h) from Harvester h where h.source.id = :id or h.target.id = :id", Long.class)
				.setParameter("id", repository.getId()).getSingleResult() > 0;
	}

	private <T> TypedQuery<T> paging(TypedQuery<T> query, Integer rows, Integer start) {
		if (rows != null) {
			query.setMaxResults(rows);
		}
		if (start != null) {
			query.setFirstResult(start);
		}
		return query;
	}
}
