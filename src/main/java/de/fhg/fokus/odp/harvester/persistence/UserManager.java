package de.fhg.fokus.odp.harvester.persistence;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.PartitionManager;
import org.picketlink.idm.model.basic.User;
import org.picketlink.idm.query.IdentityQuery;
import org.picketlink.idm.query.IdentityQueryBuilder;

@ApplicationScoped
@Named
public class UserManager {

	@Inject
	private PartitionManager partitionManager;

	public User getUser(String userId) {
		IdentityManager identityManager = partitionManager.createIdentityManager();
				
		IdentityQueryBuilder queryBuilder = identityManager.getQueryBuilder();
		IdentityQuery<User> query = queryBuilder.createIdentityQuery(User.class);
		
		query.where(queryBuilder.equal(User.ID, userId));
		
		List<User> result = query.getResultList();
		return result.isEmpty() ? null : result.get(0);
	}
	
}
