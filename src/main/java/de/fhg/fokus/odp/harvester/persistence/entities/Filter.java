package de.fhg.fokus.odp.harvester.persistence.entities;


import javax.persistence.*;

@Entity
public class Filter {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String attribute;

	private String value;

	//The position of the filter in the order of execution
	private int index;

	//Flag indicating if filter is enabled
	private boolean isFilterEnabled = true;

	//If set to false this value is treated as logical OR, AND if true.
	//These binary operands bind to the attribute of the filters predecessor and this.attribute.
	private boolean andOp = false;

	private boolean isNegation = false;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public boolean getAndOp() {
		return andOp;
	}

	public void setAndOp(boolean andOp) {
		this.andOp = andOp;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isNegation() {
		return isNegation;
	}

	public void setNegation(boolean negation) {
		isNegation = negation;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public boolean isFilterEnabled() {
		return isFilterEnabled;
	}

	public void setFilterEnabled(boolean filterEnabled) {
		isFilterEnabled = filterEnabled;
	}
}