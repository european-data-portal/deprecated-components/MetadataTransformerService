package de.fhg.fokus.odp.harvester.persistence.entities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import de.fhg.fokus.odp.harvester.persistence.enums.RunState;

@Entity
@NamedQueries({
        @NamedQuery(name = "runs_by_harvester_id", query = "select r from Run r where r.harvester.id = :id order by r.startTime desc"),
        @NamedQuery(name = "runs_with_status", query = "select r from Run r where r.harvester.id = :id and r.runState = :runstatus order by r.startTime desc"),
        @NamedQuery(name = "count_runs_with_status", query = "select count(r) from Run r where r.harvester.id = :id and r.runState = :runstatus"),
        @NamedQuery(name = "count_runs_by_harvester_id", query = "select count(r) from Run r where r.harvester.id = :id"),
        @NamedQuery(name = "completed_runs_by_date", query = "select r from Run r where r.harvester.id = :id " +
                "and (r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.error " +
                "or r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.interrupted " +
                "or r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.finished) " +
                "order by r.startTime desc"),
        @NamedQuery(name = "completed_runs_by_status", query = "select r from Run r where r.harvester.id = :id " +
                "and (r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.error " +
                "or r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.interrupted " +
                "or r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.finished) " +
                "order by r.runState desc"),
        @NamedQuery(name = "count_completed_runs", query = "select count(r) from Run r where r.harvester.id = :id " +
                "and (r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.error " +
                "or r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.interrupted " +
                "or r.runState = de.fhg.fokus.odp.harvester.persistence.enums.RunState.finished)")
})
public class Run {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private RunState runState;

    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;

    @OneToMany(mappedBy = "run", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @OrderBy("created desc")
    private List<Log> logs = new ArrayList<>();

    private Integer numberToProcess = 0;

    private Integer numberAdded = 0;

    private Integer numberUpdated = 0;

    private Integer numberSkipped = 0;

    private Integer numberRejected = 0;

    private Integer numberDeleted = 0;

    private boolean incremental;

    @ManyToOne
    private Harvester harvester;

    public Harvester getHarvester() {
        return harvester;
    }

    public void setHarvester(Harvester harvester) {
        this.harvester = harvester;
    }

    public List<Log> getLogs() {
        return this.logs;
    }

    public void setLogs(List<Log> logs) {
        this.logs = logs;
    }

    public RunState getRunState() {
        return this.runState;
    }

    public void setRunState(RunState runState) {
        this.runState = runState;
    }

    public Integer getNumberAdded() {
        return numberAdded;
    }

    public void setNumberAdded(Integer numberAdded) {
        this.numberAdded = numberAdded;
    }

    public Integer getNumberUpdated() {
        return numberUpdated;
    }

    public void setNumberUpdated(Integer numberUpdated) {
        this.numberUpdated = numberUpdated;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getDayOfRunStart() {
        return new SimpleDateFormat("yyyy-MM-dd").format(startTime);
    }

    public String getTimeWithoutDay(Date time) {
        return new SimpleDateFormat("HH:mm:ss").format(time);
    }


    public Long getRunDurationInMinutes() {
        if (startTime == null || endTime == null) {
            return 0L;
        }
        return (endTime.getTime() - startTime.getTime()) / (1000 * 60);
    }

    public Integer getNumberToProcess() {
        return numberToProcess;
    }

    public void setNumberToProcess(Integer numberToProcess) {
        this.numberToProcess = numberToProcess;
    }

    public Integer getNumberRejected() {
        return numberRejected;
    }

    public void setNumberRejected(Integer numberRejected) {
        this.numberRejected = numberRejected;
    }

    public Integer getNumberSkipped() {
        return numberSkipped;
    }

    public void setNumberSkipped(Integer numberSkipped) {
        this.numberSkipped = numberSkipped;
    }

    public Integer getNumberDeleted() {
        return numberDeleted;
    }

    public void setNumberDeleted(Integer numberDeleted) {
        this.numberDeleted = numberDeleted;
    }

    public boolean isIncremental() {
        return incremental;
    }

    public void setIncremental(boolean incremental) {
        this.incremental = incremental;
    }

}
