package de.fhg.fokus.odp.harvester.persistence.enums;

public enum ActivityType {
	user_create, user_update,
	repository_create, repository_update, repository_delete,
	harvester_create, harvester_update, harvester_delete, harvest_date_updated,
	mapping_updated
}
