package de.fhg.fokus.odp.harvester.persistence.enums;

public enum Frequency {
	every_five_minutes("Every_five_minutes"), hourly("Hourly"), daily("Daily"), weekly("Weekly"), monthly("Monthly"), yearly("Yearly"), manually("Manually");

	private final String label;

	Frequency(String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}
}
