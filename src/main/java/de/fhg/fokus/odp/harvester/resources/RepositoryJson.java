package de.fhg.fokus.odp.harvester.resources;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import de.fhg.fokus.odp.harvester.persistence.enums.RepositoryType;
import de.fhg.fokus.odp.harvester.persistence.enums.Visibility;

@JsonInclude(Include.NON_NULL)
public class RepositoryJson {

	private Integer id;
	
	private String name;
	
	private String description;
	
	private String url;

	private String originUrl;

	private Visibility visibility;
	
	private RepositoryType type;
	
	private String language;
	
	private String homepage;
	
	private String publisher;
	
	private String publisherEmail;
	
	private boolean incremental;
	
	private final List<Integer> sourceHarvester = new ArrayList<>();
	
	private final List<Integer> targetHarvester = new ArrayList<>();

	private final Set<String> spatial = new HashSet<>();
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}

	public RepositoryType getType() {
		return type;
	}

	public void setType(RepositoryType type) {
		this.type = type;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPublisherEmail() {
		return publisherEmail;
	}

	public void setPublisherEmail(String publisherEmail) {
		this.publisherEmail = publisherEmail;
	}

	public boolean isIncremental() {
		return incremental;
	}

	public void setIncremental(boolean incremental) {
		this.incremental = incremental;
	}

	public List<Integer> getSourceHarvester() {
		return sourceHarvester;
	}

	public List<Integer> getTargetHarvester() {
		return targetHarvester;
	}

	public Set<String> getSpatial() {
		return spatial;
	}

	public String getOriginUrl() {
		return originUrl;
	}

	public void setOriginUrl(String originUrl) {
		this.originUrl = originUrl;
	}

}
