package de.fhg.fokus.odp.harvester.resources;

import java.util.Date;

import de.fhg.fokus.odp.harvester.persistence.enums.RunState;

public class RunJson {

	private Integer id;
	
	private RunState status;
	
	private Date startTime;
	
	private Date endTime;
	
	private Integer harvester;
	
	private Integer numberToProcess;
	
	private Integer numberAdded;
	
	private Integer numberUpdated;
	
	private Integer numberSkipped;
	
	private Integer numberRejected;

	private Integer numberDeleted;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RunState getStatus() {
		return status;
	}

	public void setStatus(RunState status) {
		this.status = status;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getHarvester() {
		return harvester;
	}

	public void setHarvester(Integer harvester) {
		this.harvester = harvester;
	}

	public Integer getNumberToProcess() {
		return numberToProcess;
	}

	public void setNumberToProcess(Integer numberToProcess) {
		this.numberToProcess = numberToProcess;
	}

	public Integer getNumberAdded() {
		return numberAdded;
	}

	public void setNumberAdded(Integer numberAdded) {
		this.numberAdded = numberAdded;
	}

	public Integer getNumberUpdated() {
		return numberUpdated;
	}

	public void setNumberUpdated(Integer numberUpdated) {
		this.numberUpdated = numberUpdated;
	}

	public Integer getNumberSkipped() {
		return numberSkipped;
	}

	public void setNumberSkipped(Integer numberSkipped) {
		this.numberSkipped = numberSkipped;
	}

	public Integer getNumberRejected() {
		return numberRejected;
	}

	public void setNumberRejected(Integer numberRejected) {
		this.numberRejected = numberRejected;
	}

	public Integer getNumberDeleted() {
		return numberDeleted;
	}

	public void setNumberDeleted(Integer numberDeleted) {
		this.numberDeleted = numberDeleted;
	}

}
