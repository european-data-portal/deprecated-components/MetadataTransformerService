package de.fhg.fokus.odp.harvester.resources.impl;

import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import de.fhg.fokus.odp.harvester.persistence.HarvestersManager;
import de.fhg.fokus.odp.harvester.persistence.LogsManager;
import de.fhg.fokus.odp.harvester.persistence.RunsManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.entities.Log;
import de.fhg.fokus.odp.harvester.persistence.entities.Run;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.resources.HarvesterJson;
import de.fhg.fokus.odp.harvester.resources.HarvesterResource;
import de.fhg.fokus.odp.harvester.resources.LogJson;
import de.fhg.fokus.odp.harvester.resources.ResponseJson;
import de.fhg.fokus.odp.harvester.resources.RunJson;
import java.io.Serializable;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SessionScoped
public class HarvesterResourceImpl implements Serializable, HarvesterResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8509963194307934335L;

	@Inject
	private HarvestersManager harvestersManager;
	
	@Inject
	private RunsManager runsManager;

	@Inject
	private LogsManager logsManager;
	
	@Override
	@Transactional
	public ResponseJson listHarvesters(Integer rows, Integer start, String search) {
		List<Harvester> harvesters = search != null ? harvestersManager.listHarvesters(rows, start, search, false) : harvestersManager.list(rows, start);
		List<HarvesterJson> harvesterList = harvesters.stream().map(this::convertToJson).collect(Collectors.toList());
		long total = search != null ? harvestersManager.countHarvesters(search, false) : harvestersManager.count();
		return ResponseJson.createSuccess(harvesterList, total, rows, start);
	}

	@Override
	@Transactional
	public ResponseJson getHarvester(String id) {
		Harvester harvester = harvestersManager.find(Integer.parseInt(id));
		return ResponseJson.createSuccess(convertToJson(harvester), null, null, null);
	}

	@Override
	@Transactional
	public ResponseJson listHarvesterRuns(String id, Integer rows, Integer start) {
		List<Run> runs = runsManager.listRuns(Integer.parseInt(id), rows, start);
		List<RunJson> runList = runs.stream().map(this::convertToJson).collect(Collectors.toList());
		return ResponseJson.createSuccess(runList, Integer.toUnsignedLong(runsManager.countRuns(Integer.parseInt(id))), rows, start);
	}

	@Override
	@Transactional
	public ResponseJson getRun(String id, String runid) {
		Run run = runsManager.find(Integer.parseInt(runid));
		return ResponseJson.createSuccess(convertToJson(run), null, null, null);
	}

	@Override
	@Transactional
	public ResponseJson listLogs(String id, String runid, String logSeverity, Integer rows, Integer start) {
		Run run = runsManager.find(Integer.parseInt(runid));
		LogEnums.Severity severity = logSeverity == null ? null : LogEnums.Severity.valueOf(logSeverity);
		List<Log> logs = run.getLogs();
		Stream<Log> stream = logs.stream();
		if (severity != null) {
			stream = stream.filter(l -> l.getSeverity() == severity);
		}
		long count = stream.count();
		stream = logs.stream();
		if (severity != null) {
			stream = stream.filter(l -> l.getSeverity() == severity);
		}
		if (start != null) {
			stream = stream.skip(start);
		}
		if (rows != null)  {
			stream = stream.limit(rows);
		}
		List<LogJson> logList = stream.map(this::convertToJson).collect(Collectors.toList());
		return ResponseJson.createSuccess(logList, count, rows, start);
	}

	@Override
	@Transactional
	public String getAttachment(String id, String runid, String logid) {
		Log log = logsManager.find(Integer.parseInt(logid));
		return log != null ? log.getAttachment() : null;
	}

	private HarvesterJson convertToJson(Harvester harvester) {
		HarvesterJson harvesterJson = new HarvesterJson();
		harvesterJson.setId(harvester.getId());
		harvesterJson.setName(harvester.getName());
		harvesterJson.setDescription(harvester.getDescription());
		harvesterJson.setFrequency(harvester.getFrequency());
		harvesterJson.setSource(harvester.getSource().getId());
		harvesterJson.setTarget(harvester.getTarget().getId());
		harvesterJson.setScheduled(harvester.getNextHarvestDate());
		harvesterJson.setScript(harvester.getMappingScript());
		return harvesterJson;
	}
	
	private RunJson convertToJson(Run run) {
		RunJson runJson = new RunJson();
		runJson.setId(run.getId());
		runJson.setStatus(run.getRunState());
		runJson.setStartTime(run.getStartTime());
		runJson.setEndTime(run.getEndTime());
		runJson.setHarvester(run.getHarvester().getId());
		runJson.setNumberToProcess(run.getNumberToProcess());
		runJson.setNumberAdded(run.getNumberAdded());
		runJson.setNumberUpdated(run.getNumberUpdated());
		runJson.setNumberSkipped(run.getNumberSkipped());
		runJson.setNumberRejected(run.getNumberRejected());
		runJson.setNumberDeleted(run.getNumberDeleted());		
		return runJson;
	}

	private LogJson convertToJson(Log log) {
		LogJson logJson = new LogJson();
		logJson.setId(log.getId());
		logJson.setMessage(log.getMessage());
		logJson.setSeverity(log.getSeverity());
		logJson.setCategory(log.getCategory());
		logJson.setStage(log.getServiceStage());
		logJson.setCreated(log.getCreated());
		logJson.setRun(log.getRun().getId());
		logJson.setAttachment(log.getAttachment() != null);
		return logJson;
	}
	
}
