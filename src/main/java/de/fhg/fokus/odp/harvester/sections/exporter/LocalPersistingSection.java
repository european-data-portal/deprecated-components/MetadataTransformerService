package de.fhg.fokus.odp.harvester.sections.exporter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.fhg.fokus.odp.harvester.persistence.DatasetsManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Dataset;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;
import de.fhg.fokus.odp.harvester.sections.QueuedJobSection;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by sim on 10.01.2017.
 */
@Dependent
public class LocalPersistingSection extends JobSection {

    @Inject
    private DatasetsManager datasetsManager;

    public Void call() {
        log.info("Starting local persisting section.");

        Integer catalog = run.getHarvester().getSource().getId();

        ObjectMapper m = new ObjectMapper();
        QueuedJobSection<ObjectNode> queuedSection = (QueuedJobSection<ObjectNode>) attached;

        try {
            ObjectNode dataset = queuedSection.take();
            while(!queuedSection.isFinished(dataset)) {
                Dataset d;
                String name = dataset.path("name").textValue();
                List<Dataset> list = datasetsManager.findByNameAndCatalog(name, catalog);
                if (list.size() > 1) {
                    run.addLogWarning("Two datasets with same name in same catalog: (" + name + " - " + catalog + ")", ServiceStage.export_stage, LogEnums.Category.dataset);
                    for (Dataset ds : list) {
                        datasetsManager.remove(ds);
                    }
                    list.clear();
                }
                try {
                    if (list.isEmpty()) {
                        d = new Dataset();
                        d.setName(name);
                        d.setCatalog(catalog);
                        d.setContent(m.writerWithDefaultPrettyPrinter().writeValueAsString(dataset));
                        datasetsManager.persist(d);
                        run.incrementAdded();
                    } else {
                        d = list.get(0);
                        d.setPreviousContent(d.getContent());
                        d.setContent(m.writerWithDefaultPrettyPrinter().writeValueAsString(dataset));
                        datasetsManager.update(d);
                        run.incrementUpdated();
                    }
                } catch (JsonProcessingException e) {
                    log.error("pretty print dataset", e);
                    run.incrementRejected();
                }

                dataset = queuedSection.take();
            }

            // queue in again for all other threads working on this queue
            queuedSection.finish();

            log.info("Finished export section for {}.", run.getHarvester().getName());
            run.addLogInfo("Finished export section. " + queuedSection.takeCounter() + " datasets processed.", ServiceStage.export_stage, LogEnums.Category.system);

        } catch (InterruptedException e) {
            log.warn("Export thread interrupted", e);
            run.addLogWarning("Export section interrupted.", ServiceStage.export_stage, LogEnums.Category.system);
        }
        return null;
    }

}
