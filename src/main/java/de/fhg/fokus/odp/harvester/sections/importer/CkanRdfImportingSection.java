package de.fhg.fokus.odp.harvester.sections.importer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.http.HttpConnector;
import de.fhg.fokus.odp.harvester.http.ckan.CkanError;
import de.fhg.fokus.odp.harvester.http.ckan.CkanResponse;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;
import de.fhg.fokus.odp.harvester.service.RunHandler;
import de.fhg.fokus.odp.harvester.util.CkanUtil;
import de.fhg.fokus.odp.harvester.util.JenaUtils;
import org.apache.http.Header;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.shared.JenaException;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.RDF;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;

import javax.enterprise.context.Dependent;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Dependent
//@Typed(CkanRdfImportingSection.class)
public class CkanRdfImportingSection extends ImportingSection<Document> {

	private static final Document POISON = new Document();

	protected HttpConnector<ObjectNode> httpClient;

	protected AtomicInteger read = new AtomicInteger();

	private Set<String> identifiers = new HashSet<>();
    private List<String> names = new ArrayList<>();

	public CkanRdfImportingSection() {
		super(Document.class);
	}

	@Override
	protected Document poisonObject() {
		return POISON;
	}

	@Override
	public boolean init(RunHandler run, JobSection section, int capacity) {
		if (!super.init(run, section, capacity)) {
			return false;
		}

		Header[] headers = CkanUtil.addApiKeyHeader(run.getHarvester().getSourceApiKey(), null);
		httpClient = HttpConnector.createConnector(run.getHarvester().getSource().getFormattedUrl(), headers, ObjectNode.class);

		try {
			fetchNames();
			run.setNumberToProcess(names.size());
		} catch (IOException | ProtocolException e) {
			log.error("fetching identifiers", e);
			return false;
		}
		return true;
	}

	@Override
	public Void call() {
		log.info("Starting import section for {}.", run.getHarvester().getName());

		try {
			for (String name : names) {
				String request = run.getHarvester().getSource().getUrl() + "/dataset/" + name + ".rdf";
				Model dataset;
				try {
					dataset = ModelFactory.createDefaultModel().read(request);
					dataset.setNsPrefixes(JenaUtils.DCATAP_PREFIXES);
					read.getAndIncrement();
				} catch (org.apache.jena.atlas.web.HttpException e) {
					log.error("model read", e);
					run.addLogError("(" + e.getMessage() + ") while reading model from " + request, ServiceStage.import_stage, LogEnums.Category.dataset, null);
					run.incrementRejected();
					continue;
				} catch (org.apache.jena.riot.RiotException e) {
					log.error("model read", e);
					run.addLogError("(" + e.getMessage() + ") while reading model from " + request, ServiceStage.import_stage, LogEnums.Category.dataset, null);
					run.incrementRejected();
					if (e.getMessage().contains("E302")) {
						throw new InterruptedException(e.getMessage());
					}
					continue;
				}

				log.debug("Fetched dataset {}", name);

				Model m = null;
				ResIterator it = dataset.listResourcesWithProperty(RDF.type, DCAT.Dataset);
				while (it.hasNext()) {
                    Resource res = it.nextResource();
                    identifiers.add(res.getURI());
                    if (dataset.contains(res, RDF.type, DCAT.CatalogRecord)) {
                    	dataset.remove(dataset.createStatement(res, RDF.type, DCAT.CatalogRecord));
					}
                    m = JenaUtils.extractResource(res);
                }

                if (m != null) {
					try {
						if (log.isTraceEnabled()) {
							ByteArrayOutputStream output = new ByteArrayOutputStream();
							RDFDataMgr.write(output, m, RDFFormat.RDFXML_ABBREV);
							log.trace(output.toString());
						}

						Document doc = JenaUtils.asDocument(m);
						put(doc);
					} catch (JenaException e) {
						log.error("model read", e);
						run.addLogError("(" + e.getMessage() + ") while reading model from " + request, ServiceStage.import_stage, LogEnums.Category.dataset, null);
						run.incrementRejected();
					} catch (JDOMException | IOException e) {
						log.error("building document from model", e);
						run.incrementRejected();
						run.addLogError(e.getMessage(), ServiceStage.import_stage, LogEnums.Category.dataset, null);
					}
				} else {
					log.error("could not extract dataset from source model: {}", request);
					run.incrementRejected();
					run.addLogError("Could not extract dataset from source model: " + request, ServiceStage.import_stage, LogEnums.Category.dataset, null);
				}

				dataset.close();

				if (Thread.interrupted()) {
					break;
				}
			}

			finish();

			log.info("Finished import section for {}.", run.getHarvester().getName());
			run.addLogInfo("Finished import section. " + read.get() + " datasets read, " + putCounter() + " datasets imported.", ServiceStage.import_stage, LogEnums.Category.system);

		} catch (InterruptedException e) {
			log.warn("Import thread interrupted", e);
			run.addLogWarning("Import section interrupted.", ServiceStage.import_stage, LogEnums.Category.system);
		}
		return null;
	}
	
	@Override
	public String getDatasetUrl(Document dataset) {
		String ckanName = dataset.getRootElement().getChild("Dataset", Namespace.getNamespace(DCAT.NS)).getChildText("ckan-name", Namespace.getNamespace("http://data.europa.eu/euodp/ontologies/ec-odp#"));
		return run.getHarvester().getSource().getFormattedUrl() + "/dataset/" + (ckanName != null ? ckanName + ".rdf" : "");
	}

	private void fetchNames() throws IOException, ProtocolException {
        String request = "/api/action/package_list?offset=0"; // offset just set cause some portals have problems when params are missing
        JsonNode result = httpClient.get(request);

        CkanResponse ckanResponse = new CkanResponse(result);
        if (ckanResponse.isError()) {
            CkanError ckanError = ckanResponse.getError();
            throw ckanError.asException();
        } else if (ckanResponse.isSuccess()) {
            JsonNode resultContent = ckanResponse.getResult().getContent();
            if (resultContent.isArray()) {
                resultContent.forEach(n -> names.add(n.textValue()));
            } else {
                names.clear();
            }
        } else {
            throw new ProtocolException("unknown response format:\n" + new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(result));
        }

    }

	@Override
	public Set<String> listIdentifiers() {
		return identifiers;
	}

}
