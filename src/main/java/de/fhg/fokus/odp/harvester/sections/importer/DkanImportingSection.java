package de.fhg.fokus.odp.harvester.sections.importer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.fhg.fokus.odp.harvester.cdi.Specialized;
import de.fhg.fokus.odp.harvester.exceptions.GeneralHttpException;
import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by sim on 12.05.2017.
 */
@Dependent
@Specialized
public class DkanImportingSection extends CkanImportingSection {

    private Set<String> identifiers;

    protected String buildRequest(int start) {
        return "/api/3/action/current_package_list_with_resources?limit=" + NUMBER_OF_BATCH_ROWS + "&offset=" + start;
    }

    public Void call() {
        log.info("Starting import section for {}.", run.getHarvester().getName());

        identifiers = new HashSet<>();

        try {
//            JsonNode response = httpClient.get("/api/3/action/package_list");
//            JsonNode result = response.path("result");
//            if (result.isArray()) {
//                job.setNumberToProcess(result.size());
//            }

            int offset = 0;
            List<ObjectNode> page;
            do {
                page = nextPage(offset);
                for (ObjectNode dataset : page) {
                    put(dataset);
                }
                offset += page.size();

            } while (!page.isEmpty());

            finish();

        } catch (GeneralHttpException e) {
            failed("(" + e.getStatusCode() + " " + e.getMessage() + ") " + e.getBody(), e);
        } catch (ProtocolException | IOException e) {
            failed(e.getMessage(), e);
        } catch (InterruptedException e) {
            log.warn("Import thread interrupted", e);
            run.addLogWarning("Import section interrupted.", ServiceStage.import_stage, LogEnums.Category.system);
            return null;
        }

        log.info("Finished import section for {}.", run.getHarvester().getName());
        run.addLogInfo("Finished import section. " + read.get() + " datasets read, " + putCounter() + " datasets imported.", ServiceStage.import_stage, LogEnums.Category.system);

        return null;
    }

    protected List<ObjectNode> parseResult(JsonNode result) {
        ArrayList<ObjectNode> list = new ArrayList<>();
        if (result.isArray()) {
            read.addAndGet(result.size());
            list.ensureCapacity(result.size());
            for (JsonNode element : result) {
                list.add((ObjectNode) element);
                String name = element.get("name").asText();
                if (identifiers.contains(name)) {
                    log.warn("Dataset {} already imported.", name);
                    run.addLogWarning("Dataset " + name + " already imported.", ServiceStage.import_stage, LogEnums.Category.dataset);
                } else {
                    identifiers.add(name);
                }
            }
        }
        return list;
    }

    @Override
    public Set<String> listIdentifiers() throws IOException, ProtocolException {
        return identifiers;
    }

}
