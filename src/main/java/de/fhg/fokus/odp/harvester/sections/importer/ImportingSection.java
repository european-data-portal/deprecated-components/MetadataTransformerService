package de.fhg.fokus.odp.harvester.sections.importer;

import java.io.IOException;
import java.util.Set;

import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.http.HttpConnector;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;
import de.fhg.fokus.odp.harvester.sections.QueuedJobSection;
import de.fhg.fokus.odp.harvester.service.RunHandler;
import de.fhg.fokus.odp.harvester.util.CkanUtil;
import org.apache.http.Header;

public abstract class ImportingSection<I> extends QueuedJobSection<I> {

	private Class<I> clazz;

	public String getDatasetUrl(I dataset) { 
		return run.getHarvester().getSource().getFormattedUrl();
	}
	
	public abstract Set<String> listIdentifiers() throws IOException, ProtocolException;

	protected HttpConnector<I> httpClient;

	protected ImportingSection(Class<I> clazz) {
		this.clazz = clazz;
	}

	public boolean init(RunHandler run, JobSection section, int capacity) {
		if (!super.init(run, section, capacity)) {
			return false;
		}
		Header[] headers = CkanUtil.addApiKeyHeader(run.getHarvester().getSourceApiKey(), null);
		httpClient = HttpConnector.createConnector(run.getHarvester().getSource().getFormattedUrl(), headers, clazz);
		return httpClient != null && super.init(run, section);
	}

	public void close() {
		if (httpClient != null) {
			httpClient.close();
		}
	}

	protected void failed(String message, Exception e) {
		super.failed(message, e, ServiceStage.import_stage);
		log.error("Import section for " + run.getHarvester().getName() + " failed.", e);
		run.addLogError(e.getMessage(), ServiceStage.import_stage, LogEnums.Category.connection, null);
	}

}
