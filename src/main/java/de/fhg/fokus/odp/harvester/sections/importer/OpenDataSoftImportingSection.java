package de.fhg.fokus.odp.harvester.sections.importer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.fhg.fokus.odp.harvester.cdi.Specialized;
import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.http.opendatasoft.OpenDataSoftError;
import de.fhg.fokus.odp.harvester.http.opendatasoft.OpenDataSoftResponse;
import de.fhg.fokus.odp.harvester.http.opendatasoft.OpenDataSoftResult;
import de.fhg.fokus.odp.harvester.persistence.entities.Filter;
import de.fhg.fokus.odp.harvester.persistence.entities.Run;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Dependent
@Specialized
public class OpenDataSoftImportingSection extends CkanImportingSection {

    private final Set<String> identifiers = new HashSet<>();

    @Override
    protected String buildRequest(int start) {
        String sourceKey = run.getHarvester().getSourceApiKey();
        String apiKeyRequest = sourceKey == null || sourceKey.isEmpty() ? "" : "apikey=" + sourceKey + "&";

        return "/api/v2/catalog/datasets?"
                + apiKeyRequest
                + "rows=" + NUMBER_OF_BATCH_ROWS
                + "&start=" + start;
    }

    @Override
    protected void prepareFilter() {

        List<Filter> filters = run.getHarvester().getEnabledAndSortedFilters();

        StringBuilder fsb = new StringBuilder("q=");

        if (run.getHarvester().getSource().isIncremental()) {
            Run latest = run.getLatestRun();
            if (latest != null) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                fsb.append("modified%20>=%20").append(dateFormat.format(latest.getStartTime()));
            }
        }

        //Build filter String
        if (!filters.isEmpty()) {
            run.addLogInfo("Number of filters: " + filters.size(), ServiceStage.filter_stage, LogEnums.Category.system);
            for (Filter filter : filters) {
                try {
                    fsb.append(filter.getAndOp() ? "%20AND%20" : "%20OR%20");

                    if (filter.isNegation()) {
                        fsb.append("NOT%20"); 	//Preceding space is taken care of by sb append above
                    }

                    fsb.append(URLEncoder.encode(filter.getAttribute() + ":" + filter.getValue(), "utf-8"));
                } catch (UnsupportedEncodingException e) {
                    log.error("url encoding filter", e);
                }
            }
        }

        filterString = fsb.toString();

        if (filterString.trim().isEmpty()) {
            run.addLogInfo("No filter to use.", ServiceStage.filter_stage, LogEnums.Category.system);
        } else {
            run.addLogInfo("Final filter to use: " + filterString, ServiceStage.filter_stage, LogEnums.Category.system);
        }
        log.debug("filter prepared");
    }

    @Override
    protected List<ObjectNode> nextPage(int start) throws IOException, ProtocolException {
        String request = buildRequest(start);
        JsonNode response = httpClient.get(request);

        OpenDataSoftResponse openDataSoftResponse = new OpenDataSoftResponse(response);

        if (openDataSoftResponse.isError()) {
            OpenDataSoftError openDataSoftError = openDataSoftResponse.getError();
            throw openDataSoftError.asException();
        } else if (openDataSoftResponse.isSuccess()) {
            OpenDataSoftResult openDataSoftResult = openDataSoftResponse.getResult();
            return parseResult(openDataSoftResult.getContent());
        } else {
            throw new ProtocolException("unknown response format:\n" + new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(response));
        }
    }

    @Override
    protected List<ObjectNode> parseResult(JsonNode result) {
/*
        JsonNode links = result.path("links");

        if (links.isArray() && links.size() == 1) {
            JsonNode linkNode = links.get(0);
            pageRequest = !linkNode.path("next").isMissingNode() ? result.path("next").textValue() : "";
        }
*/

        run.setNumberToProcess(result.path("total_count").asInt());
        JsonNode upstreamDatasets = result.path("datasets");
        ArrayList<ObjectNode> resultDatasets = new ArrayList<>();

        //Check if array contains datasets
        if (upstreamDatasets.isArray()) {
            //Reserve space
            resultDatasets.ensureCapacity(upstreamDatasets.size());
            for (JsonNode node : upstreamDatasets) {

                //Remove wrapper
                JsonNode actualDataset = node.path("dataset");
                identifiers.add(actualDataset.path("dataset_id").textValue());
                resultDatasets.add((ObjectNode) actualDataset);
            }
        }
        return resultDatasets;
    }

    @Override
    public String getDatasetUrl(ObjectNode dataset) {
        return run.getHarvester().getSource().getFormattedUrl() + "/api/v2/catalog/datasets/" + dataset.path("dataset_id").textValue();
    }

    @Override
    public Set<String> listIdentifiers() {
        return identifiers;
    }
}
