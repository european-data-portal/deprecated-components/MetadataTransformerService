package de.fhg.fokus.odp.harvester.sections.importer;

import de.fhg.fokus.odp.harvester.exceptions.GeneralHttpException;
import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.util.xml.DadosGovPtRssXmlDocumentHandler;

import org.jdom2.Document;
import org.jdom2.Namespace;

import javax.enterprise.context.Dependent;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by sim on 20.09.2015.
 */
@Dependent
public class RssImportingSection extends ImportingSection<Document> {

    private static final String DATASET_URL_HOST = "http://www.dados.gov.pt";
    private static final String DATASET_URL_PATH = "/PT/CatalogoDados/Dados.aspx?name=";

	private static Document POISON = new Document();
	
    protected final Set<String> identifiers = new HashSet<>();

	public RssImportingSection() {
		super(Document.class);
	}

	@Override
	protected Document poisonObject() {
		return POISON;
	}

	@Override
	public Void call() {
        try {
			Document result = httpClient.get("");
			if (result != null) {
				List<Document> datasets = parseResult(result);

				for (Document dataset : datasets) {
					put(dataset);
				}

			} else {
				log.error("invalid response from source");
				run.addLogError("Importing datasets: Invalid response from source.", ServiceStage.import_stage, LogEnums.Category.dataset, null);
			}

			finish();

		} catch(GeneralHttpException e) {
			failed("(" + e.getStatusCode() + " " + e.getMessage() + ") " + e.getBody(), e);
        } catch (IOException | ProtocolException e) {
			failed(e.getMessage(), e);
        } catch (InterruptedException e) {
			log.warn("Import thread interrupted", e);
			run.addLogWarning("Import section interrupted.", ServiceStage.import_stage, LogEnums.Category.system);
			return null;
		}

		log.info("Finished import section for {}.", run.getHarvester().getName());
		run.addLogInfo("Finished import section. " + putCounter() + " datasets imported.", ServiceStage.import_stage, LogEnums.Category.system);

		return null;
	}
	
	private List<Document> parseResult(Document result) {
		DadosGovPtRssXmlDocumentHandler handler = DadosGovPtRssXmlDocumentHandler.createHandler(result);
		List<Document> records = handler.getRecords();
		for (Document record : records) {
	    	Namespace atom = Namespace.getNamespace("http://www.w3.org/2005/Atom");
	    	Namespace metadata = Namespace.getNamespace("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata");
	    	Namespace dataservices = Namespace.getNamespace("http://schemas.microsoft.com/ado/2007/08/dataservices");
	    	String name = record.getRootElement().getChild("content", atom).getChild("properties", metadata).getChildTextTrim("name", dataservices);
			identifiers.add(name);
		}
		run.setNumberToProcess(records.size());
		return records;
	}

    @Override
    public String getDatasetUrl(Document dataset) {
    	Namespace atom = Namespace.getNamespace("http://www.w3.org/2005/Atom");
    	Namespace metadata = Namespace.getNamespace("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata");
    	Namespace dataservices = Namespace.getNamespace("http://schemas.microsoft.com/ado/2007/08/dataservices");
    	String name = dataset.getRootElement().getChild("content", atom).getChild("properties", metadata).getChildTextTrim("name", dataservices);
        return DATASET_URL_HOST + DATASET_URL_PATH + name;
    }

	@Override
	public Set<String> listIdentifiers() {
		return identifiers;
	}
    
}
