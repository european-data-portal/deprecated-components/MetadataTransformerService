package de.fhg.fokus.odp.harvester.sections.importer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.fhg.fokus.odp.harvester.cdi.Specialized;
import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.http.socrata.SocrataError;
import de.fhg.fokus.odp.harvester.http.socrata.SocrataResponse;
import de.fhg.fokus.odp.harvester.http.socrata.SocrataResult;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Dependent
@Specialized
public class SocrataImportingSection extends CkanImportingSection {

    private String pageRequest = null;

    private final Set<String> identifiers = new HashSet<>();

    @Override
    protected String buildRequest(int start) {
        if (pageRequest == null) {
            //Order by name to prevent duplicates while paging. Ordering by id threw an error
            pageRequest = "/api/catalog/v1?only=datasets&limit=" + NUMBER_OF_BATCH_ROWS + "&offset=" + start + "&order=name";
        }
        return pageRequest;
    }

    @Override
    protected void prepareFilter() {
        //remove log clutter
    }

    @Override
    protected List<ObjectNode> nextPage(int start) throws IOException, ProtocolException {
        String request = buildRequest(start);
        JsonNode response = httpClient.get(request);

        SocrataResponse socrataResponse = new SocrataResponse(response);

        if (socrataResponse.isError()) {
            SocrataError socrataError = socrataResponse.getError();
            throw socrataError.asException();
        } else if (socrataResponse.isSuccess()) {
            SocrataResult socrataResult = socrataResponse.getResult();
            return parseResult(socrataResult.getContent());
        } else {
            throw new ProtocolException("unknown response format:\n" + new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(response));
        }
    }

    @Override
    protected List<ObjectNode> parseResult(JsonNode result) {

        //Socrata catalog API does not have 'count'
        run.setNumberToProcess(0);
        ArrayList<ObjectNode> resultDatasets = new ArrayList<>();

        ArrayNode results = (ArrayNode) result.path("results");
        if (results != null) {
            read.addAndGet(result.size());
            resultDatasets.ensureCapacity(results.size());
            for (JsonNode element : results) {
                resultDatasets.add((ObjectNode) element);
                identifiers.add(element.path("id").textValue());
            }
        }

        return resultDatasets;
    }

    @Override
    public String getDatasetUrl(ObjectNode dataset) {
        return dataset.path("permalink").textValue();
    }

    @Override
    public Set<String> listIdentifiers() {
        return identifiers;
    }

}