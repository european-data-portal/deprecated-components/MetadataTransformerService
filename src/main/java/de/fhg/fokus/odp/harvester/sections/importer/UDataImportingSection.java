package de.fhg.fokus.odp.harvester.sections.importer;

import de.fhg.fokus.odp.harvester.cdi.Specialized;
import de.fhg.fokus.odp.harvester.exceptions.GeneralHttpException;
import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.http.udata.UDataError;
import de.fhg.fokus.odp.harvester.http.udata.UDataResponse;
import de.fhg.fokus.odp.harvester.http.udata.UDataResult;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.Dependent;

/**
 * Import Client for uData API
 * Created by jpi on 22.07.2015.
 */
@Dependent
@Specialized
public class UDataImportingSection extends CkanImportingSection {

	private String pageRequest = null;

	private final Map<String, ObjectNode> cache = new HashMap<>();

	private final Set<String> identifiers = new HashSet<>();

	@Override
	protected String buildRequest(int start) {
		if (pageRequest == null) {
			pageRequest = "/api/1/datasets/?page_size=" + NUMBER_OF_BATCH_ROWS + "&page=1";
		}
        return pageRequest;
	}
	
	@Override
	protected void prepareFilter() {
		// get rid of misleading filter logs
	}
	
    @Override
    protected List<ObjectNode> nextPage(int start) throws IOException, ProtocolException {
    	String request = buildRequest(start);
    	JsonNode response = httpClient.get(request);
    	UDataResponse udataResponse = new UDataResponse(response);
    	if (udataResponse.isError()) {
    		UDataError udataError = udataResponse.getError();
    		throw udataError.asException();
    	} else if (udataResponse.isSuccess()) {
    		UDataResult udataResult = udataResponse.getResult();
    		return parseResult(udataResult.getContent());
    	} else {
    		throw new ProtocolException("unknown response format:\n" + new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(response));
    	}
    }
    
    @Override
	protected List<ObjectNode> parseResult(JsonNode result) {

		if (!result.path("next_page").isMissingNode() && !result.path("next_page").isNull()) {
			pageRequest = result.path("next_page").textValue().replace(run.getHarvester().getSource().getFormattedUrl(), "");
		}
		run.setNumberToProcess(result.path("total").asInt());
		JsonNode array = result.path("data");
		ArrayList<ObjectNode> list = new ArrayList<>();
        if (array.isArray()) {
        	read.addAndGet(array.size());
            list.ensureCapacity(array.size());
            for (JsonNode node : array) {
            	identifiers.add(node.path("slug").textValue());
            	if (completeSpatialInfo(node)) {
                    list.add((ObjectNode) node);            		
            	} else {
					run.incrementRejected();
            		String dataset;
					try {
						dataset = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(node);
					} catch (JsonProcessingException e) {
						log.error("pretty print dataset", e);
						dataset = "[" + e.getMessage() + "]";
					}
            		run.addLogError("Could not fetch appropriate spatial value for zones in " + node.path("slug").textValue(), ServiceStage.import_stage, LogEnums.Category.dataset, dataset);
            	}
            }
        }
        return list;
	}

	private boolean completeSpatialInfo(JsonNode node) {
		JsonNode zones = node.path("spatial").path("zones");
		if (zones.isArray() && zones.size() > 0) {
			for (JsonNode zone : zones) {
	        	String zoneId = zone.textValue();
	        	ObjectNode spatial;
	        	if (cache.containsKey(zoneId)) {
	        		spatial = cache.get(zoneId);
					log.debug("getting spatial value for {} from cache", zoneId);
	        	} else {
	        		spatial = getGeoJsonObjectFromId(zoneId);
					cache.put(zoneId, spatial);
	        	}

	        	if (spatial != null) {
					String spatialText;
					try {
						spatialText = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(spatial);
						if (spatialText.getBytes().length <= 32766) {
							((ObjectNode) node).set("spatial", spatial);
							break;
						} else {
							log.debug("spatial value exceeds size limit of 32766: {}", spatialText.getBytes().length);
						}
					} catch (JsonProcessingException e) {
						log.error("pretty print spatial", e);
					}
				}
			}
			return !node.path("spatial").path("coordinates").isMissingNode();
		} else {
			log.debug("no zone info in dataset, accepting as is.");
			return true;
		}
	}
	
    /**
     * Gets the GeoJSON coordinates from a spatial ID
     * @param id ID
     * @return GeoJSON coordinates
     */
    private ObjectNode getGeoJsonObjectFromId(String id) {
        String request;
		try {
			request = "/api/1/spatial/zones/" + URLEncoder.encode(id, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.error("encoding geo zone", e);
			run.addLogError("Encoding zone: " + e.getMessage(), ServiceStage.import_stage, LogEnums.Category.dataset, null);
			return null;
		}
		try {
			ObjectNode response = httpClient.get(request);
			JsonNode features = response.path("features");
			if (features.isArray()) {
				return (ObjectNode) features.path(0).path("geometry");
			}
		} catch (GeneralHttpException e) {
			log.error("getting geo json", e);
			run.addLogError("(" + e.getStatusCode() + " " + e.getMessage() + ") " + e.getBody(), ServiceStage.export_stage, LogEnums.Category.dataset, null);
		} catch (ProtocolException | IOException e) {
			log.error("getting geo json", e);
			run.addLogError("Getting geo json: " + e.getMessage(), ServiceStage.import_stage, LogEnums.Category.connection, null);
		}
		return null;
    }
    
    @Override
    public String getDatasetUrl(ObjectNode dataset) {
    	if (dataset.has("uri")) {
        	return dataset.path("uri").textValue();
    	} else {
    		return run.getHarvester().getSource().getFormattedUrl() + "/datasets/" + dataset.path("slug").textValue();
    	}
    }
    
	@Override
	public Set<String> listIdentifiers() {
		return identifiers;    	
	}

}
