package de.fhg.fokus.odp.harvester.sections.transformer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collections;

import javax.enterprise.context.Dependent;
import javax.xml.transform.TransformerException;

import de.fhg.fokus.odp.harvester.sections.importer.ImportingSection;
import de.fhg.fokus.odp.harvester.service.RunHandler;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;
import de.fhg.fokus.odp.harvester.transformer.scripttransformer.XslScriptTransformer;

@Dependent
public class JsonRdfToJsonTransformingSection extends TransformingSection<ObjectNode, ObjectNode> {

	private static final ObjectNode POISON = new ObjectMapper().createObjectNode();
	
	private XslScriptTransformer transformer;
	
	@Override
	public ObjectNode poisonObject() {
		return POISON;
	}
	
	public boolean init(RunHandler run, JobSection section, int capacity) {
		if (super.init(run, section, capacity)) {
			transformer = new XslScriptTransformer(script, Collections.singletonMap("repo_lang", run.getHarvester().getSource().getLanguage().getLanguageKey()));
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public ObjectNode transform(ObjectNode dataset) {
    	JsonNode rdf = dataset.path("rdf");
    	if (rdf.isMissingNode() || rdf.isNull() || rdf.textValue().trim().isEmpty()) {
			try {
				String d = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(dataset);
				log.error("no rdf field or field is empty: {}", d);
				run.addLogError("Transformation error: No rdf field or field is empty", ServiceStage.transformation_stage, LogEnums.Category.dataset, d);
			} catch (JsonProcessingException e) {
				log.error("json pretty print", e);
			}
			run.incrementRejected();
			return null;
    		
    	}
    	
		SAXBuilder builder = new SAXBuilder();
		builder.setIgnoringElementContentWhitespace(true);
		builder.setIgnoringBoundaryWhitespace(true);
		Document rdfDoc;
		try {
			rdfDoc = builder.build(new ByteArrayInputStream(rdf.textValue().getBytes()));
		} catch (JDOMException | IOException e) {
			try {
				String d = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(dataset);
				log.error("parsing stream: {}", d, e);
				run.addLogError("Transformation error: " + e.getMessage(), ServiceStage.transformation_stage, LogEnums.Category.dataset, d);
			} catch (JsonProcessingException e1) {
				log.error("json pretty print", e1);
			}
			run.incrementRejected();
			return null;
		}
    	
        try {
        	String transformed = transformer.transform(rdfDoc);
        	if (transformed.trim().isEmpty()) {
    			String d = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(dataset);
    			log.error("Transformation results is empty. This implies that the rdf field contains no or invalid dcat:Dataset element: {}", d);
    			run.addLogError("Transformation results is empty. This implies that the rdf field contains no or invalid dcat:Dataset element.", ServiceStage.transformation_stage, LogEnums.Category.transformation, d);
    			run.incrementRejected();
    			return null;
        	}
        	
        	ObjectMapper mapper = new ObjectMapper();
        	mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
        	mapper.configure(Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
        	mapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        	
			ObjectNode target = (ObjectNode) mapper.readTree(transformed);
			target.set("name", dataset.get("name"));
			String originalSource = ((ImportingSection<ObjectNode>) attached).getDatasetUrl(dataset);
	        if (originalSource != null) {
	            target.put("original_source", originalSource);
	        }				

	        return target;
		} catch (TransformerException | IOException e) {
			try {
				String d = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(dataset);
				log.error("transforming dataset: {}", d, e);
				run.addLogError(e.getMessage(), ServiceStage.transformation_stage, LogEnums.Category.transformation, d);
			} catch (JsonProcessingException e1) {
				log.error("json pretty print", e1);
			}
			run.incrementRejected();
		}

        return null;
	}

}
