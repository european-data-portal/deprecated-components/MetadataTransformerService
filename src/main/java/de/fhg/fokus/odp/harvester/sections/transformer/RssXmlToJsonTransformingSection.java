package de.fhg.fokus.odp.harvester.sections.transformer;

import javax.enterprise.context.Dependent;

import de.fhg.fokus.odp.harvester.sections.importer.ImportingSection;
import org.jdom2.Document;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.fhg.fokus.odp.harvester.cdi.Specialized;

@Dependent
@Specialized
public class RssXmlToJsonTransformingSection  extends XmlToJsonTransformingSection {

	@Override
	public ObjectNode transform(Document dataset) {
		ObjectNode transformed = super.transform(dataset);
		if (transformed != null) {
			ObjectMapper mapper = new ObjectMapper();
	        ObjectNode resource = mapper.createObjectNode();
	        resource.put("format", "HTML");
	        ArrayNode urlArray = mapper.createArrayNode();
	        urlArray.add(((ImportingSection<Document>) attached).getDatasetUrl(dataset));
	        resource.set("access_url", urlArray);

	        ArrayNode resourceArray = mapper.createArrayNode();
	        resourceArray.add(resource);
	        transformed.set("resources", resourceArray);
		}
		return transformed;
	}
	
}
