package de.fhg.fokus.odp.harvester.sections.transformer;

import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.enums.HarvesterEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;
import de.fhg.fokus.odp.harvester.sections.QueuedJobSection;
import de.fhg.fokus.odp.harvester.sections.importer.ImportingSection;
import de.fhg.fokus.odp.harvester.service.RunHandler;
import de.fhg.fokus.odp.harvester.transformer.scripttransformer.XslScriptTransformer;
import de.fhg.fokus.odp.harvester.util.TransformationScriptDownloader;

public abstract class TransformingSection<I, E> extends QueuedJobSection<E> {

    public abstract E transform(I dataset);

    protected String script;

    public boolean init(RunHandler run, JobSection section, int capacity) {
        if (!super.init(run, section, capacity)) {
            return false;
        }
        Harvester harvester = run.getHarvester();
        if (harvester.getTransformationScriptSource() == HarvesterEnums.TransformationScriptSource.custom) {
            script = harvester.getMappingScript();
        } else if (harvester.getTransformationScriptSource() == HarvesterEnums.TransformationScriptSource.git) {
            TransformationScriptDownloader.GitRepository git = new TransformationScriptDownloader.GitRepository(
                    harvester.getGitRepoUrl(),
                    harvester.getGitBranch(),
                    harvester.getGitFileName(),
                    harvester.isGitRepoRequiresCredentials(),
                    harvester.isGitRepoSshAuthentication(),
                    harvester.getGitUserName(),
                    harvester.getGitPassWord());
            script = new TransformationScriptDownloader().initScriptDownloader(git).getCurrentScript(true);
        }

        return true;
    }

    public Void call() {
        log.info("Starting transform section.");
        ImportingSection<I> importingSection = (ImportingSection<I>) attached;
        try {
            I input = importingSection.take();
            while (!importingSection.isFinished(input)) {
                E output = transform(input);
                if (output != null) {
                    put(output);
                }
                input = importingSection.take();
            }

            importingSection.finish();
            finish();

            log.info("Finished transform section. Transformed {} datasets.", putCounter());
            run.addLogInfo("Finished transform section. " + putCounter() + " of " + importingSection.takeCounter() + " datasets transformed.", ServiceStage.transformation_stage, LogEnums.Category.system);

        } catch (InterruptedException e) {
            log.warn("Transformer thread interrupted", e);
            run.addLogWarning("Transform section interrupted.", ServiceStage.transformation_stage, LogEnums.Category.system);
        }
        return null;
    }
    
}
