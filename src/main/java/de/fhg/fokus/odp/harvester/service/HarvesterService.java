package de.fhg.fokus.odp.harvester.service;

import de.fhg.fokus.odp.harvester.persistence.HarvestersManager;
import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.entities.Log;
import de.fhg.fokus.odp.harvester.persistence.entities.Run;
import de.fhg.fokus.odp.harvester.persistence.enums.*;
import de.fhg.fokus.odp.harvester.service.scheduler.QuartzSchedulerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@ApplicationScoped
public class HarvesterService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private HarvestersManager harvestersManager;

	@Inject
	private QuartzSchedulerService quartzSchedulerService;

	public void schedule(Harvester harvester) {
		Date date = quartzSchedulerService.scheduleJob(harvester.getId().toString(), harvester.getFrequency(), harvester.getNextHarvestDate());
		if (date != null) {
			harvester.setNextHarvestDate(date);
		}

		log.info("Harvester '{}' set next schedule time: {}", harvester.getName(), harvester.getNextHarvestDate().toString());
	}

	@Transactional
	public void initializeHarvesters() {
		for (Harvester harvester : harvestersManager.list(null, null)) {
			for (Run run : harvester.getRuns()) {
				switch (run.getRunState()) {
				case running:
				case scheduled:

					String errorMsg = "Illegal state during app initialization";

					run.setRunState(RunState.error);
					Log log = new Log();
					log.setSeverity(LogEnums.Severity.error);
					log.setRun(run);
					log.setServiceStage(ServiceStage.init_stage);
					log.setMessage(errorMsg);
					log.setCategory(log.parseMsgForCategory(errorMsg));

					run.getLogs().add(log);
					break;
				default:
				}
			}
			
			if (harvester.getFrequency() != Frequency.manually) {
				schedule(harvester);
			}
		}
	}
	
}
