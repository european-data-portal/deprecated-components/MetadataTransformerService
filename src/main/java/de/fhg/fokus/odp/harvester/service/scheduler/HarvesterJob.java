package de.fhg.fokus.odp.harvester.service.scheduler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import de.fhg.fokus.odp.harvester.application.AppConfiguration;
import de.fhg.fokus.odp.harvester.exceptions.ProtocolException;
import de.fhg.fokus.odp.harvester.persistence.enums.LogEnums;

import de.fhg.fokus.odp.harvester.persistence.enums.RunState;
import de.fhg.fokus.odp.harvester.sections.exporter.*;
import de.fhg.fokus.odp.harvester.sections.importer.*;
import de.fhg.fokus.odp.harvester.sections.transformer.*;
import de.fhg.fokus.odp.harvester.service.RunHandler;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.UnableToInterruptJobException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fhg.fokus.odp.harvester.cdi.Specialized;
import de.fhg.fokus.odp.harvester.exceptions.GeneralHttpException;
import de.fhg.fokus.odp.harvester.persistence.entities.Harvester;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import de.fhg.fokus.odp.harvester.sections.JobSection;
import de.fhg.fokus.odp.harvester.util.EMailService;

@DisallowConcurrentExecution
@Dependent
public class HarvesterJob implements InterruptableJob {

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	private Thread thread;

	@Resource(name="metadataTransformerService")
	private ManagedExecutorService executorService;

	@Inject
	private AppConfiguration appConfiguration;

	@Inject
	private EMailService emailService;
	
	@Inject
	private Instance<CkanImportingSection> ckanImporter;

	@Inject
	@Specialized
	private Instance<DkanImportingSection> dkanImporter;

	@Inject
	private Instance<CkanRdfImportingSection> ckanRdfImporter;

	@Inject
	private Instance<OaiPmhImportingSection> oaiPmhImporter;
	
	@Inject
	@Specialized
	private Instance<UDataImportingSection> uDataImporter;
	
	@Inject
	private Instance<JsonLDDumpImportingSection> jsonLdDumpImporter;
	
	@Inject
	private Instance<RssImportingSection> rssImporter;
	
	@Inject
	@Specialized
	private Instance<OpenDataSoftImportingSection> openDataSoftImporter;

	@Inject
	@Specialized
	private Instance<SocrataImportingSection> socrataImporter;
	
	@Inject
	private Instance<DCIPImportingSection> dcipImporter;
	
	@Inject
	private Instance<RDFImportingSection> rdfImporter;

	@Inject
	private Instance<SPARQLImportingSection> sparqlImporter;

	@Inject
	private Instance<JsonToJsonTransformingSection> jsonToJsonTransformer;

	@Inject
	private Instance<XmlToJsonTransformingSection> xmlToJsonTransformer;

	@Inject
	@Specialized
	private Instance<RssXmlToJsonTransformingSection> rssXmlToJsonTransformer;

	@Inject
	private Instance<JsonRdfToJsonTransformingSection> jsonRdfToJsonTransformer;

	@Inject
	private Instance<LocalPersistingSection> localPersisting;

	@Inject
	private Instance<MemoryCollectingSection> memoryCollector;

	@Inject
	private Instance<CkanExportingSection> ckanExporter;

	@Inject
	private Instance<SPARQLExportingSection> sparqlExporter;

	@Inject
	private RunHandler handler;

	private List<Future<?>> runningFutures;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			String jobName = context.getJobDetail().getKey().getName();
			if (!jobName.endsWith(".manually") && context.getScheduler().checkExists(new JobKey(jobName + ".manually", jobName))) {
				log.warn("scheduled job execution canceled. harvester already started manually.");
				return;
			}			
		} catch (SchedulerException e) {
			log.error("checking existing running jobs", e);
			return;
		}

		thread = Thread.currentThread();
		runningFutures = new ArrayList<>();

		String id = context.getJobDetail().getKey().getGroup();

		handler.initRun(id, context);
		Harvester harvester = handler.getRun().getHarvester();

        handler.addLogInfo("Harvesting run started at " + handler.getRun().getStartTime(), ServiceStage.init_stage, LogEnums.Category.system);

        JobSection transformer;
        JobSection importer;

        switch (harvester.getSource().getRepositoryType()) {
        case ckan:
        	importer = ckanImporter.get();
        	transformer = jsonToJsonTransformer.get();
        	break;
			case dkan:
				importer = dkanImporter.get();
				transformer = jsonToJsonTransformer.get();
				break;
        case dcatap:
        	importer = oaiPmhImporter.get();
        	transformer = xmlToJsonTransformer.get();
        	break;
        case udata:
        	importer = uDataImporter.get();
        	transformer = jsonToJsonTransformer.get();
        	break;
        case rdf:
        	importer = rdfImporter.get();
        	transformer = xmlToJsonTransformer.get();
        	break;
		case sparql:
			importer = sparqlImporter.get();
			transformer = xmlToJsonTransformer.get();
			break;
        case rss:
        	importer = rssImporter.get();
        	transformer = rssXmlToJsonTransformer.get();
        	break;
        case rdf_ckan:
        	importer = ckanRdfImporter.get();
        	transformer = xmlToJsonTransformer.get();
        	break;
        case jsonld_dump:
        	importer = jsonLdDumpImporter.get();
        	transformer = jsonToJsonTransformer.get();
        	break;
		case opendatasoft:
			importer = openDataSoftImporter.get();
			transformer = jsonToJsonTransformer.get();
			break;
		case socrata:
			importer = socrataImporter.get();
			transformer = jsonToJsonTransformer.get();
			break;
		case dcip:
			importer = dcipImporter.get();
			transformer = jsonToJsonTransformer.get();
			break;
        default:
			handler.addLogError("Importer not implemented.", ServiceStage.init_stage, LogEnums.Category.system, null);
			handler.setState(RunState.error);
        	return;
        }

		if (!((ImportingSection)importer).init(handler, null, appConfiguration.getQueuesCapacity())) {
        	log.error("Initialization of importer failed.");
			handler.addLogError("Initialization of importer failed.", ServiceStage.init_stage, LogEnums.Category.system, null);
			handler.setState(RunState.error);
    		return;
    	}

    	if (!((TransformingSection)transformer).init(handler, importer, appConfiguration.getQueuesCapacity())) {
			log.error("Initialization of transformer failed.");
			handler.addLogError("Initialization of transformer failed.", ServiceStage.init_stage, LogEnums.Category.system, null);
    		importer.close();
			handler.setState(RunState.error);
    		return;
    	}

        JobSection exporter;
        switch (harvester.getTarget().getRepositoryType()) {
        case ckan:
        	exporter = ckanExporter.get();
        	break;
		case sparql:
			exporter = sparqlExporter.get();
			transformer = new TransformingSection<Model, Model>() {

				private Model poison = ModelFactory.createDefaultModel();

				@Override
				protected Model poisonObject() {
					return poison;
				}

				@Override
				public Model transform(Model dataset) {
					return dataset;
				}
			};
			break;
		case memory:
			exporter = memoryCollector.get();
			break;
		case local_persist:
			exporter = localPersisting.get();
			break;
		default:
			handler.addLogError("Exporter not implemented.", ServiceStage.init_stage, LogEnums.Category.system, null);
			importer.close();
			transformer.close();
			handler.setState(RunState.error);
			return;
        }

    	if (!exporter.init(handler, transformer)) {
			log.error("Initialization of exporter failed.");
			handler.addLogError("Initialization of exporter failed", ServiceStage.init_stage, LogEnums.Category.system, null);
    		importer.close();
    		transformer.close();
			handler.setState(RunState.error);
    		return;
    	}

    	ExecutorCompletionService<Void> completion = new ExecutorCompletionService<>(executorService);

		runningFutures.add(completion.submit(importer));
		runningFutures.add(completion.submit(transformer));
		runningFutures.add(completion.submit(exporter));

		while (!runningFutures.isEmpty()) {
			try {
				Future<Void> future = completion.take();
				runningFutures.remove(future);
				future.get();
			} catch (InterruptedException e) {
				log.warn("callable interrupted", e);
			} catch (CancellationException e) {
				log.warn("callable canceled", e);
			} catch (ExecutionException e) {
				log.error("callable exception", e);
				for (Future<?> f : runningFutures) {
					f.cancel(true);
				}
				runningFutures.clear();
				if (handler.getRun().getRunState() == RunState.running) {
					handler.setState(RunState.error);
				}
			}
		}

		if (handler.getRun().getRunState() == RunState.running) {
			// delete section
			try {
				if (exporter instanceof CkanExportingSection) {
					handler.addLogInfo("Preparing the deletion phase, fetching all identifiers from source (may take a while...)", ServiceStage.delete_stage, LogEnums.Category.system);
					Set<String> identifiers = ((ImportingSection<?>) importer).listIdentifiers();
					if (!identifiers.isEmpty()) {
						((CkanExportingSection) exporter).postDelete(identifiers);
					} else {
						handler.addLogInfo("No identifiers available, skipping deletion phase.", ServiceStage.delete_stage, LogEnums.Category.system);
					}
				}
			} catch (GeneralHttpException e) {
				log.error("post delete", e);
				handler.addLogError("(" + e.getStatusCode() + " " + e.getMessage()+ ") " + e.getBody(), ServiceStage.delete_stage, LogEnums.Category.connection, null);
				if (harvester.getEmailNotification()) {
					emailService.sendError(handler.getRun(), e, ServiceStage.delete_stage);
				}
			} catch (ProtocolException | IOException e) {
				log.error("post delete", e);
				handler.addLogError(e.getMessage(), ServiceStage.delete_stage, LogEnums.Category.connection, null);
				if (harvester.getEmailNotification()) {
					emailService.sendError(handler.getRun(), e, ServiceStage.delete_stage);
				}
			}
		}

		importer.close();
		transformer.close();
		exporter.close();

		if (handler.getRun().getRunState() == RunState.running) {
			handler.setState(RunState.finished);
		}

		handler.addLogInfo("Harvesting run finished at " + handler.getRun().getEndTime(), ServiceStage.init_stage, LogEnums.Category.system);
	}

	public RunHandler getRun() {
		return handler;
	}

	public Harvester getHarvester() {
		return handler.getHarvester();
	}

	@Override
	public void interrupt() throws UnableToInterruptJobException {
		handler.addLogWarning("Run interrupted.", ServiceStage.init_stage, LogEnums.Category.system);
		if (handler.getRun().getRunState() == RunState.running) {

			for (Future<?> future : runningFutures) {
				future.cancel(true);
			}

			handler.setState(RunState.interrupted);
//			run.setRunState(RunState.interrupted);
//			run = runsManager.update(run);
		} else {
			thread.interrupt();
		}

	}

}
