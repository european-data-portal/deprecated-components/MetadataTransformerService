package de.fhg.fokus.odp.harvester.transformer.scripttransformer;

import de.fhg.fokus.odp.harvester.exceptions.TransformationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.script.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Created by jpi on 02.06.2015.
 */
public class JavaScriptTransformer {

    /** The Constant REPO_PROPERTIES_FILENAME. */
    static final String TRANSFORMER_PROPERTIES_FILENAME = "transformer.properties";

    /** The Constant TRANSFRORMER_SERVICE_FILE_LIST */
    static final String TRANSFRORMER_SERVICE_FILE_LIST = "transformer.factory.files";

    private final Logger log = LoggerFactory.getLogger(getClass());

	ScriptEngine engine = null;

	/**
	 * Initializes the JsonTransformer using the given Harvester
	 */
	public JavaScriptTransformer(String script) {
		if (script != null) {
			// init Javascript engine
			ScriptEngineManager factory = new ScriptEngineManager();
			engine = factory.getEngineByName("JavaScript");
			ScriptContext context = engine.getContext();
			context.setAttribute("name", "JavaScript", ScriptContext.ENGINE_SCOPE);

			// load standard files
			Properties props = new Properties();
			try {
				props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(TRANSFORMER_PROPERTIES_FILENAME));

				String fileList = props.getProperty(TRANSFRORMER_SERVICE_FILE_LIST);
				for (String file : fileList.split(",")) {
					// Read script from resource
					BufferedReader reader = new BufferedReader(new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(file)));

					// Evaluate script
					engine.eval(reader);
				}

				// Evaluate script
				engine.eval(script);
			} catch (IOException | ScriptException e) {
				log.error("Error while loading standard transformation files: {}", e.getMessage());
			}
		}
	}

	/**
	 * Transforms a given JsonObject using the given transformation scripts
	 * 
	 * @param input
	 *            the input JsonObject
	 * @return the transformed JsonObject
	 * @throws TransformationException
	 */
	public ObjectNode transform(ObjectNode input) throws TransformationException {
		Invocable jsInvoke = (Invocable) engine;

		Bindings bindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);
		Object transRule = bindings.get("jsontTemplate");

		try {
			String d = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(input);

			engine.eval("var input = " + d + ";");
			Object output = jsInvoke.invokeFunction("executeTransformation", input, transRule);

			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
			mapper.configure(Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
			mapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

			return (ObjectNode) mapper.readTree(output.toString());
		} catch (IOException | NoSuchMethodException | ScriptException e) {
			log.error("transformation", e);
			throw new TransformationException(e.getMessage());
		}
	}
}
