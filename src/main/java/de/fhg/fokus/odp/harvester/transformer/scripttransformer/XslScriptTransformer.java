package de.fhg.fokus.odp.harvester.transformer.scripttransformer;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.jdom2.Document;
import org.jdom2.transform.JDOMSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class XslScriptTransformer {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private Templates templates;

	private Map<String, Object> params = new HashMap<>();

	public XslScriptTransformer(String script, Map<String, Object> params) {
		TransformerFactory transFact = TransformerFactory.newInstance();
		transFact.setURIResolver((href, base) -> {
			if (href.startsWith("http://europeandataportal.eu/edp-funct.xsl")) {
				return new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream("edp-funct.xsl"));
			}
			return null;
		});
		try {
			templates = transFact.newTemplates(new StreamSource(new ByteArrayInputStream(script.getBytes())));
			this.params = params;
		} catch (TransformerConfigurationException e) {
			log.error("initializing script template", e);
		}
	}

	public String transform(Document metadata) throws TransformerException, IOException {
		Transformer transformer = templates.newTransformer();
		params.forEach(transformer::setParameter);

		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			transformer.transform(new JDOMSource(metadata), new StreamResult(out));
			return out.toString("UTF-8");			
		}
	}
}
