package de.fhg.fokus.odp.harvester.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.activation.DataHandler;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import de.fhg.fokus.odp.harvester.application.AppConfiguration;
import de.fhg.fokus.odp.harvester.exceptions.GeneralHttpException;
import de.fhg.fokus.odp.harvester.persistence.entities.Run;
import de.fhg.fokus.odp.harvester.persistence.enums.ServiceStage;
import org.picketlink.idm.model.basic.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import de.fhg.fokus.odp.harvester.persistence.UserManager;

@ApplicationScoped
public class EMailService {

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Resource(name = "java:jboss/mail/EDP")
    private Session session;
	
	@Inject
	private UserManager userManager;
	
	private TemplateEngine templateEngine;

	@Inject
	private AppConfiguration appConfiguration;

	@PostConstruct
	public void init() {
		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setTemplateMode("HTML");
        templateResolver.setPrefix("/emails/");
        templateResolver.setSuffix(".html");
        
		templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);
	}
	
	public boolean send(Map<String, Object> parameters, String userId) {
		User user = userManager.getUser(userId);
		return send(parameters, user);
	}

	public boolean send(Map<String, Object> parameters, User user) {
		if (session == null || user.getEmail() == null || user.getEmail().isEmpty()) {
			return false;
		}
		
		Context ctx = new Context();
		ctx.setVariables(parameters);
		ctx.setVariable("firstname", user.getFirstName());
		ctx.setVariable("lastname", user.getLastName());

		ctx.setVariable("logo", "logo");

		String content = templateEngine.process("simple", ctx);
		log.info("email: {}", content);
		
        try {
            Message message = new MimeMessage(session);
            String personal = user.getFirstName();
            if (user.getLastName() != null && !user.getLastName().isEmpty()) {
            	personal = String.join(" ", personal, user.getLastName());
            }
            Address address = null;
            if (personal != null && !personal.isEmpty()) {
            	try {
					address = new InternetAddress(user.getEmail(), personal);
				} catch (UnsupportedEncodingException e) {
					log.error("address invalid", e);
				}
            } else {
            	address = new InternetAddress(user.getEmail());            	
            }
            message.setFrom(new InternetAddress("noreply@europeandataportal.eu"));
            message.setRecipient(Message.RecipientType.TO, address);
            message.setSubject("[EDP - " + appConfiguration.getInstance() + "] ["+ parameters.get("sourceType") + "] MetadataTransformerService");
            
            MimeMultipart multipart = new MimeMultipart("related");
            
            MimeBodyPart html = new MimeBodyPart();
            html.setContent(content, "text/html; charset=utf-8");
            multipart.addBodyPart(html);

			try {
				DataHandler handler = new DataHandler(new ByteArrayDataSource(Thread.currentThread().getContextClassLoader().getResourceAsStream("/emails/EDP_LOGO_SMALL.png"), "image/png"));
	            MimeBodyPart logo = new MimeBodyPart();
	            logo.setDataHandler(handler);
	            logo.setContentID("logo");
	            multipart.addBodyPart(logo);
			} catch (IOException e) {
				log.error("set logo", e);
			}
            
            message.setContent(multipart);
 
            Transport.send(message);
 
        } catch (MessagingException e) {
            log.warn("Cannot send mail", e);
            return false;
        }
        return true;
	}

	public void sendError(Run run, Exception e, ServiceStage stage) {
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("header", "Harvesting run failed!");
		parameters.put("harvester", run.getHarvester().getName());
		parameters.put("run", run.getStartTime());
		parameters.put("sourceType", run.getHarvester().getSource().getRepositoryType().getLabel());
		if (stage != null) {
			parameters.put("stage", stage.getLabel());
		}

		if (e instanceof GeneralHttpException) {
			GeneralHttpException g =(GeneralHttpException)e;
			parameters.put("message", "" + g.getStatusCode() + " - " + g.getMessage());
			parameters.put("details", ((GeneralHttpException)e).getBody());
		} else if (e.getCause() != null) {
			parameters.put("message", e.getMessage());
			parameters.put("details", e.getCause().getMessage());
		} else {
			parameters.put("message", e.getMessage());
		}

		send(parameters, run.getHarvester().getOwner());
	}

}
