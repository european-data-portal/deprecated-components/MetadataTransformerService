package de.fhg.fokus.odp.harvester.util;

import de.fhg.fokus.odp.harvester.application.AppLocalization;

import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.*;
import org.eclipse.jgit.transport.OpenSshConfig.Host;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Session;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@RequestScoped
public class TransformationScriptDownloader {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private GitRepository gitRepository;

	// Use builder pattern to allow not storing a reference
	public TransformationScriptDownloader initScriptDownloader(GitRepository gitRepository) {
		this.gitRepository = gitRepository;
		return this;
	}

	// Returns a list of all script files stored in the local git directory
	public List<Path> listAllScripts() {
		ArrayList<Path> scriptFiles = new ArrayList<>();
		Path gitRepoDir = gitRepository.getRepoDirWithBranch();
		if (gitRepoDir != null && Files.exists(gitRepoDir)) {
			try (Stream<Path> scripts = Files.find(gitRepoDir, 20, (file, attr) -> file.toFile().getName().endsWith(".js") || file.toFile().getName().endsWith(".xslt"),
					FileVisitOption.FOLLOW_LINKS)) {
				scripts.forEach(p -> {
					scriptFiles.add(gitRepoDir.relativize(p));
				});
			} catch (IOException e) {
				AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrFailedToReadDir", null);
				log.error("Could not read local file directory " + gitRepoDir.toString(), e);
			}
		}
		return scriptFiles;
	}

	// Returns the content of the selected file
	public String getCurrentScript(boolean update) {
		if (update) {
			updateOrGetRepo();
		}
		if (gitRepository.getFileName() != null) {
			Path gitRepoDir = gitRepository.getRepoDirWithBranch();
			try {
				return new String(Files.readAllBytes(gitRepoDir.resolve(gitRepository.getFileName())));
			} catch (IOException e) {
				AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrFailedToReadScriptFile", null);
				log.error("Could not read local file {}. Returning empty String instead.", gitRepoDir.resolve(gitRepository.getFileName()));
			}
		}
		return "";
	}

	// Either clones or pulls from a Git repository
	public void updateOrGetRepo() {
		Path gitRepoDir = gitRepository.getRepoDirWithBranch();
		if (gitRepoDir != null) {

			// Use master as default branch
			String branch = gitRepository.getBranch() != null && !gitRepository.getBranch().isEmpty()
					? gitRepository.getBranch() : "master";

			if (!Files.exists(gitRepoDir)) {
				// Clone branch if not existent on disk
				log.debug("cloning remote repo from " + gitRepository.getRepoUrl() + " to " + gitRepoDir.toString());
				cloneRemoteRepo(branch);
			} else {
				// Pull branch if existent
				log.debug("pulling from upstream " + gitRepository.getRepoUrl());
				pullRemoteRepo();
			}

		}
	}

	// Clones a repository
	private void cloneRemoteRepo(String branch) {

		CloneCommand cloneCommand = Git.cloneRepository().setURI(gitRepository.getRepoUrl()).setBranch(branch)
				.setDirectory(gitRepository.getRepoDirWithBranch().toFile());

		cloneCommand = (CloneCommand) setAuthentication(cloneCommand);

		try (Git git = cloneCommand.call()) {
			if (git == null) {
				removeGitFiles();
			}
		} catch (TransportException | InvalidRemoteException e) {
			log.error("calling clone command", e);
			if (e.getMessage().contains("not authorized")) {
				AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrInvalidGitCredentials", null);
			} else {
				AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrInvalidGitRemote", null);
			}
		} catch (GitAPIException e) {
			log.error("calling clone command", e);
			AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrGitGeneral", null);
		}
	}

	// Pulls a repository, warning if merging failed
	private void pullRemoteRepo() {
		try (Repository localRepo = new FileRepository(
				gitRepository.getRepoDirWithBranch() + File.separator + ".git")) {
			try (Git repo = new Git(localRepo)) {
				PullCommand pullCommand = repo.pull();
				pullCommand = (PullCommand) setAuthentication(pullCommand);
				
				PullResult pResult = pullCommand.call();
				MergeResult mResult = pResult.getMergeResult();

				if (!mResult.getMergeStatus().isSuccessful()) {
					String errorMsg = "could not merge repository: " + mResult.toString();
					log.warn(errorMsg);
					AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrMergeFailed", null);
				}
			}
		} catch (TransportException e) {
			log.error("calling pull command", e);
			if (e.getMessage().contains("not authorized")) {
				AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrInvalidGitCredentials", null);
			}
		} catch (IOException | GitAPIException e) {
			log.error("calling pull command", e);
			AppLocalization.showMessage(FacesMessage.SEVERITY_ERROR, "msgErrGitGeneral", null);
		}
	}

	// Removes the entire git repository directory, including the branches within
	public void removeGitFiles() {
		if (gitRepository.getRepoBaseDir() != null) {
			try {
				Files.walkFileTree(gitRepository.getRepoBaseDir(), new SimpleFileVisitor<Path>() {
					@Override
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
						Files.delete(file);
						return FileVisitResult.CONTINUE;
					}

					@Override
					public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
						if (e == null) {
							Files.delete(dir);
							return FileVisitResult.CONTINUE;
						} else {
							// directory iteration failed
							throw e;
						}
					}
				});
			} catch (IOException e) {
				log.error("Failed to delete git repository " + gitRepository.getRepoBaseDir(), e);
			}
		}
	}

	private TransportCommand setAuthentication(TransportCommand command) {
		// Simple password authentication
		if (gitRepository.isRequiresCredentials() && !gitRepository.isSshAuthentication()) {
			command.setCredentialsProvider(
					new UsernamePasswordCredentialsProvider(gitRepository.getUsername(), gitRepository.getPassword()));
		}
		// SSH Authentication with or without password
		else if (gitRepository.isSshAuthentication()) {
			command.setTransportConfigCallback(transport -> ((SshTransport) transport).setSshSessionFactory(new JschConfigSessionFactory() {
				@Override
				protected void configure(Host host, Session session) {
					if (gitRepository.isRequiresCredentials())
						session.setPassword(gitRepository.getPassword());
				}
			}));
		}
		return command;
	}

	public static class GitRepository {
		private String repoUrl, branch, username, password;
		private boolean requiresCredentials, sshAuthentication;
		private Path fileName;

		public GitRepository(String repoUrl, String branch, String fileName, boolean requiresCredentials, boolean sshAuthentication,
				String username, String password) {
			this.repoUrl = repoUrl;
			this.branch = branch;
			this.fileName = fileName != null ? Paths.get(fileName) : null;
			this.requiresCredentials = requiresCredentials;
			this.sshAuthentication = sshAuthentication;
			this.username = username;
			this.password = password;
		}

		public String getRepoUrl() {
			return repoUrl;
		}

		public String getBranch() {
			return branch;
		}

		public Path getFileName() {
			return fileName;
		}

		public boolean isRequiresCredentials() {
			return requiresCredentials;
		}

		public boolean isSshAuthentication() {
			return sshAuthentication;
		}

		public void setSshAuthentication(boolean sshAuthentication) {
			this.sshAuthentication = sshAuthentication;
		}

		public String getUsername() {
			return username;
		}

		public String getPassword() {
			return password;
		}

		public boolean isValid() {
			return !(repoUrl == null || repoUrl.isEmpty() || branch == null || branch.isEmpty());
		}

		public Path getRepoBaseDir() {
			/*
			 * Script files are stored in wildfly_tmp_dir/modified_url The final
			 * directory name is the url stripped of special characters
			 */
			return isValid() ? Paths.get(System.getProperty("jboss.server.temp.dir") + File.separator
					+ "transformation_scripts" + File.separator).resolve(repoUrl.replaceAll("[^\\w\\s]", "")) : null;
		}

		public Path getRepoDirWithBranch() {
			return getRepoBaseDir() != null ? getRepoBaseDir().resolve(branch) : null;
		}
	}
}