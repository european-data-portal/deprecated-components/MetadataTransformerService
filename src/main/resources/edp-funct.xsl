<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:edp="http://europeandataportal.eu/funct"
    xmlns:functx="http://www.functx.com">

    <xsl:include href="http://www.xsltfunctions.com/xsl/functx-1.0-nodoc-2007-01.xsl"/>

    <xsl:variable name="fr" select="('\t', '\n', '\r', '\\', '&quot;')"/>
    <xsl:variable name="to" select="('\\t', '\\n', '\\r', '\\\\', '\\&quot;')"/>

    <xsl:function name="edp:select-property">
        <xsl:param name="property" />
        <xsl:sequence select="functx:replace-multi(normalize-space($property[1]), $fr, $to)" />
    </xsl:function>

    <xsl:function name="edp:select-single-lang">
        <xsl:param name="property" />
        <xsl:param name="lang" />
        <xsl:choose>
            <xsl:when test="$property[not(@xml:lang)]">
                <xsl:sequence select="edp:select-property(($property[not(@xml:lang)])[1])" />
            </xsl:when>
            <xsl:when test="$property[@xml:lang = $lang]">
                <xsl:sequence select="edp:select-property(($property[@xml:lang = $lang])[1])" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:sequence select="edp:select-property(($property)[1])" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <xsl:function name="edp:select-as-literal">
        <xsl:param name="property" />
        <xsl:choose>
            <xsl:when test="$property[@rdf:resource]">
                <xsl:value-of select="$property[@rdf:resource]"/>
            </xsl:when>
            <xsl:when test="$property[rdfs:label]">
                <xsl:apply-templates select="$property/rdfs:label"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$property" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

</xsl:stylesheet>